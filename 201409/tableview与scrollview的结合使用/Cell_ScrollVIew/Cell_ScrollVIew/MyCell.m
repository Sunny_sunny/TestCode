//
//  MyCell.m
//  Cell_ScrollVIew
//
//  Created by administrator on 14-9-1.
//  Copyright (c) 2014年 bwj. All rights reserved.
//

#import "MyCell.h"
#define kImageViewCount 3

NSMutableArray *imageViewArray;
UIImageView *imageView1;
UIImageView *imageView2;
UIImageView *imageView3;
@implementation MyCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark 获取scrollView上的三个ImageView
-(void)initImageView{
    NSArray *subviews = [self.imageShow subviews];
    NSLog(@"subviews-------%d",subviews.count);
    imageView1 = [subviews objectAtIndex:0];
    imageView2 = [subviews objectAtIndex:1];
    imageView3 = [subviews objectAtIndex:2];
}




#pragma mark 初始化cell上的scrollView
-(void)setImageScrollVIew:(NSArray *)imageArray imageIndex:(int)imageIndex{
    
    CGFloat width = self.imageShow.frame.size.width;
    CGFloat height = self.imageShow.frame.size.height;
    int count = imageArray.count;
    
    //如果scrollView上已经有了三个imageView，直接更改它们上面的图片，如果没有则创建
    if ([self.imageShow subviews].count != 3) {
        //初始化scrollView的三个ImageView
        for (int i = 0; i < kImageViewCount; i++) {
            UIImageView *imageView = [[UIImageView alloc] init];
            
            imageView.frame = CGRectMake(i * width, 0, width, height);
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            
            int imageNum = i - 1;
            if (imageNum < 0) {
                imageNum = imageArray.count - 1;
            }
            
            [imageViewArray addObject:imageView];
            imageView.image = [UIImage imageNamed:imageArray[imageNum % count]];
            [self.imageShow addSubview:imageView];
        }
        
        
        self.imageShow.contentSize = CGSizeMake(kImageViewCount * width, height);
        
        self.imageShow.showsHorizontalScrollIndicator = NO;
        self.imageShow.showsVerticalScrollIndicator = NO;
        
        self.imageShow.pagingEnabled = YES;
        
        
    }else{
        [self initImageView];
        int imageNum = imageIndex - 1;
        if (imageNum < 0) {
            imageNum = imageArray.count - 1;
        }
        imageView1.image = [UIImage imageNamed:imageArray[imageNum % count]];
        imageView2.image = [UIImage imageNamed:imageArray[imageIndex % count]];
        imageView3.image = [UIImage imageNamed:imageArray[(imageIndex + 1) % count]];
    }
    
    self.imageShow.contentOffset = CGPointMake(width, 0);
}


#pragma mark 滑动时，更新scrollView上的图片

-(void)updateImages:(NSArray *)imageArray index:(int)index direction:(int)direction{
    NSLog(@"updateImage---------");
    //获得scrollView上的三个imageView
    [self initImageView];
    
    int count = imageArray.count;
    
    if (direction == 1) {//向左滑动
        NSLog(@"%@",imageArray[(index % count)]);
        imageView1.image = [UIImage imageNamed:imageArray[(index % count)]];
        imageView2.image = [UIImage imageNamed:imageArray[((index + 1) % count)]];
        imageView3.image = [UIImage imageNamed:imageArray[((index + 2) % count)]];
//        self.imageShow.contentOffset = CGPointMake(self.imageShow.frame.size.width, 0);
    }
    if (direction == 0) {//向右滑动
        imageView1.image = [UIImage imageNamed:imageArray[(index - 2) % count]];
        imageView2.image = [UIImage imageNamed:imageArray[((index - 1) % count)]];
        imageView3.image = [UIImage imageNamed:imageArray[index % count]];
        
    }
    self.imageShow.contentOffset = CGPointMake(self.imageShow.frame.size.width, 0);
    
}


@end











