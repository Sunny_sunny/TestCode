//
//  BWJAppDelegate.h
//  Cell_ScrollVIew
//
//  Created by administrator on 14-9-1.
//  Copyright (c) 2014年 bwj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BWJAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
