//
//  MainViewController.m
//  Cell_ScrollVIew
//
//  Created by administrator on 14-9-1.
//  Copyright (c) 2014年 bwj. All rights reserved.
//

#import "MainViewController.h"
#import "ShiTiLei.h"
#import "MyCell.h"
#import "DetailViewController.h"


@interface MainViewController ()
{
    NSMutableArray *shiti;
    NSArray *images;
    NSIndexPath *selectedIndex;
}


@end

@implementation MainViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"精选房屋";
   
    
    //图片数组
    NSArray *images1 = @[@"0.jpeg",@"1.jpeg",@"2.jpeg",@"3.jpeg"];
    NSArray *images2 = @[@"010.png",@"011.png",@"012.png",@"013.png"];
    NSArray *house1 = @[@"01.jpg",@"03.jpg",@"06.jpg",@"07.jpg"];
    NSArray *house2 = @[@"02.jpg",@"05.jpg",@"08.jpg",@"09.jpg",@"10.jpg"];
    NSArray *house3 = @[@"04.jpg",@"11.jpg",@"12.jpg",@"13.jpg"];
    NSArray *sea_house = @[@"sea1.jpg",@"sea2.jpg",@"sea3.jpg",@"sea4.jpg",@"sea5.jpg"];
    images = @[images1,images2,house1,house2,house3,sea_house];
 
    ShiTiLei *s1 = [ShiTiLei shiTiLeiWithDetail:@"first" ImageArray:images[0]];
    ShiTiLei *s2 = [ShiTiLei shiTiLeiWithDetail:@"second" ImageArray:images[1]];
    ShiTiLei *h1 = [ShiTiLei shiTiLeiWithDetail:@"天朝帝都房源" ImageArray:images[2]];
    ShiTiLei *h2 = [ShiTiLei shiTiLeiWithDetail:@"亲近自然类" ImageArray:images[3]];
    ShiTiLei *h3 = [ShiTiLei shiTiLeiWithDetail:@"农家乐" ImageArray:images[4]];
//    ShiTiLei *s3 = [ShiTiLei shiTiLeiWithDetail:@"third" ImageArray:images[0]];
//    ShiTiLei *s4 = [ShiTiLei shiTiLeiWithDetail:@"fourth" ImageArray:images[1]];
//    ShiTiLei *s5 = [ShiTiLei shiTiLeiWithDetail:@"third" ImageArray:images[0]];
    
    //初始化实体类数组
    shiti = [NSMutableArray array];
    
    [shiti addObject:h1];
    [shiti addObject: h2];
    [shiti addObject:h3];
    [shiti addObject:s1];
    [shiti addObject:s2];
    
    
//    [shiti addObject:s3];
//    [shiti addObject:s4];
//    [shiti addObject:s5];
    
    //给tableView添加手势
    UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapGR.numberOfTapsRequired = 1;
    tapGR.numberOfTouchesRequired = 1;
    [self.tableView addGestureRecognizer:tapGR];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view 代理方法

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return shiti.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *CellIdentifier = @"myCell";
    MyCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    

    // Configure the cell...
    
    ShiTiLei *s = shiti[indexPath.row];
    
    cell.infoLabel.text = s.detail;
    cell.imageShow.delegate = self;

    [cell setImageScrollVIew:s.imageArray imageIndex:s.imageIndex];
    
    //用cell.tag保存当前行号
    cell.tag = indexPath.row;
    
    //用scrollView的tag来保存当前cell的行号,相当于传递的一个参数
    cell.imageShow.tag = indexPath.row;
    return cell;
}


#pragma mark 调转前传递数据
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSLog(@"%@",[sender class]);
    MyCell *cell = (MyCell *)sender;
    NSLog(@"%d",cell.tag);
    
    DetailViewController *detail = segue.destinationViewController;
    detail.detail = cell.infoLabel.text;
}

#pragma mark 单击手势处理方法
-(void)handleTap:(UITapGestureRecognizer *)recognizer{
    //前两种方法失败
    //1
//    DetailViewController *detail = [[DetailViewController alloc] init];
//    [self.navigationController pushViewController:detail animated:YES];
    //2
//    [self presentViewController:detail animated:YES completion:nil];
    
    CGPoint point = [recognizer locationInView:self.tableView];
    NSIndexPath *index = [self.tableView indexPathForRowAtPoint:point];
    MyCell *cell = (MyCell *)[self.tableView cellForRowAtIndexPath:index];
    
    [self performSegueWithIdentifier:@"detailSegue" sender:cell];
    
    
    
    
    /*
    DetailViewController *detail = [self.storyboard instantiateViewControllerWithIdentifier:@"detail"];
    detail.detail = cell.infoLabel.text;
    
    
    [self.navigationController pushViewController:detail animated:YES];
     */
}


#pragma mark scrollView代理方法

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //如果滑动是由tableView触发的，则不予以理会
    if ([scrollView isKindOfClass:[self.tableView class]]) return;

    NSLog(@"-------");
    
    //获取当前cell的行号
    int row = scrollView.tag;
    
    MyCell *cell = (MyCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
    ShiTiLei *s = shiti[row];
    
    //获取cell上显示图片的index
    int index = s.imageIndex;
    //图片数组count
    int count = s.imageArray.count;
    
    
    
    if (scrollView.contentOffset.x == 640) {
        NSLog(@"向左滑--------");
        
        //调用cell的方法，更新scrollView上的图片,1代表向左滑动
        [cell updateImages:s.imageArray index:index direction:1];
        index++;
        
        //最后再将当前的index赋给cell.imageIndex
        s.imageIndex = index % count;
        
    }
    if (scrollView.contentOffset.x == 0) {
        NSLog(@"向右滑--------");
        if ((index - 2) < 0) {
            index = index + count;
        }
        [cell updateImages:s.imageArray index:index direction:0];
        index--;
        s.imageIndex = index;
    }
    
    
}




-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    NSLog(@"x = %.2f,y = %.2f",scrollView.contentOffset.x,scrollView.contentOffset.y);
}
/*
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //判断是谁的scrollView触发的
    if ([scrollView isKindOfClass:[self.tableView class]]) return;
    if (scrollView.contentOffset.x == 640) {
        NSLog(@"640-------");
    }
    if (scrollView.contentOffset.x == 0) {
        NSLog(@"0----------");
        NSLog(@"%@",[scrollView class]);
    }
}
*/

@end












