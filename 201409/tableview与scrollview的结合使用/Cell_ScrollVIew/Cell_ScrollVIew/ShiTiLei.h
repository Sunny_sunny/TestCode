//
//  ShiTiLei.h
//  Cell_ScrollVIew
//
//  Created by administrator on 14-9-1.
//  Copyright (c) 2014年 bwj. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShiTiLei : NSObject

@property int imageIndex;
@property (copy, nonatomic) NSString *detail;
@property (retain, nonatomic) NSMutableArray *imageArray;

-(id)initWithDetail:(NSString *)detail ImageArray:(NSArray *)imageArray;
+(id)shiTiLeiWithDetail:(NSString *)detail ImageArray:(NSArray *)imageArray;

@end
