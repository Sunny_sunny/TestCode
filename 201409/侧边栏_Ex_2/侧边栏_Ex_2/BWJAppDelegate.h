//
//  BWJAppDelegate.h
//  侧边栏_Ex_2
//
//  Created by administrator on 14-9-11.
//  Copyright (c) 2014年 bwj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BWJAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
