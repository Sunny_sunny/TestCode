//
//  MenuViewController.m
//  侧边栏_Ex_2
//
//  Created by administrator on 14-9-11.
//  Copyright (c) 2014年 bwj. All rights reserved.
//

#import "MenuViewController.h"
#import "REFrostedViewController.h"
#import "UIViewController+REFrostedViewController.h"

#import "MainViewController.h"
#import "SearchViewController.h"

@interface MenuViewController ()
{
    NSMutableArray *menu;
    NSMutableArray *journeyMenu;
    NSMutableArray *hireMenu;
}

@end

@implementation MenuViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    //初始化数据
    journeyMenu = [NSMutableArray arrayWithObjects:@"搜索",@"精选",@"菜单三",@"菜单四", nil];
    hireMenu = [NSMutableArray arrayWithObjects:@"菜单一",@"菜单二",@"菜单三", nil];
    
    NSDictionary *dic1 = @{@"title": @"旅行",
                           @"menu":journeyMenu};
    NSDictionary *dic2 = @{@"title": @"出租",
                           @"menu":hireMenu};
    menu = [NSMutableArray arrayWithObjects:dic1,dic2, nil];
    
    //分隔符颜色
    self.tableView.separatorColor = [UIColor colorWithRed:150/255.0f green:161/255.0f blue:177/255.0f alpha:1.0f];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    //透明
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableHeaderView = ({
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 184.0f)];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 40, 100, 100)];
        imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        imageView.image = [UIImage imageNamed:@"0.jpeg"];
        imageView.layer.masksToBounds = YES;
        imageView.layer.cornerRadius = 50.0;
        imageView.layer.borderColor = [UIColor whiteColor].CGColor;
        imageView.layer.borderWidth = 3.0f;
        imageView.layer.rasterizationScale = [UIScreen mainScreen].scale;
        imageView.layer.shouldRasterize = YES;
        imageView.clipsToBounds = YES;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 150, 0, 24)];
        label.text = @"白文吉";
        label.font = [UIFont fontWithName:@"HelveticaNeue" size:21];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor colorWithRed:62/255.0f green:68/255.0f blue:75/255.0f alpha:1.0f];
        [label sizeToFit];
        label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        
        [view addSubview:imageView];
        [view addSubview:label];
        view;
    });

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor colorWithRed:62/255.0f green:68/255.0f blue:75/255.0f alpha:1.0f];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
}

#pragma mark 自定义Section的Header
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)sectionIndex
{
    
    //    if (sectionIndex == 0)
    //        return nil;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 34)];
    view.backgroundColor = [UIColor colorWithRed:167/255.0f green:167/255.0f blue:167/255.0f alpha:0.6f];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 8, 0, 0)];
    //    label.text = @"Friends Online";
    label.text = [menu[sectionIndex] objectForKey:@"title"];
    label.font = [UIFont systemFontOfSize:15];
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor clearColor];
    [label sizeToFit];
    [view addSubview:label];
    
    return view;
}

#pragma mark SectionHeader的高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)sectionIndex
{
    //    if (sectionIndex == 0)
    //        return 0;
    
    return 34;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UINavigationController *navigationController = (UINavigationController *)self.frostedViewController.contentViewController;
    
    //    if (indexPath.section == 0 && indexPath.row == 0) {
    //        DEMOHomeViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"homeController"];
    //        navigationController.viewControllers = @[homeViewController];
    //    } else {
    //        DEMOSecondViewController *secondViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"secondController"];
    //        navigationController.viewControllers = @[secondViewController];
    //    }
    
    
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
            {//这里的花括号不能省
                SearchViewController *search = [self.storyboard instantiateViewControllerWithIdentifier:@"searchController"];
                navigationController.viewControllers = @[search];
            }
                break;
            case 1:
            {
                MainViewController *mainvc = [self.storyboard instantiateViewControllerWithIdentifier:@"mainController"];
                navigationController.viewControllers = @[mainvc];
            }
                break;
                
                
        }
        
    }
    
    
    [self.frostedViewController hideMenuViewController];
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 54;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return menu.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [[menu[sectionIndex] objectForKey:@"menu"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    /*
     if (indexPath.section == 0) {
     NSArray *titles = @[@"Home", @"Profile", @"Chats"];
     cell.textLabel.text = titles[indexPath.row];
     } else {
     NSArray *titles = @[@"John Appleseed", @"John Doe", @"Test User"];
     cell.textLabel.text = titles[indexPath.row];
     }*/
    NSArray *temp = [menu[indexPath.section] objectForKey:@"menu"];
    cell.textLabel.text = temp[indexPath.row];
    
    return cell;
}

@end
