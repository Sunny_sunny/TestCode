//
//  DetailViewController.h
//  Cell_ScrollVIew
//
//  Created by administrator on 14-9-2.
//  Copyright (c) 2014年 bwj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController
@property (copy, nonatomic) NSString *detail;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;


@end
