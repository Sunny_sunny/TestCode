//
//  main.m
//  侧边栏_Ex_2
//
//  Created by administrator on 14-9-11.
//  Copyright (c) 2014年 bwj. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BWJAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BWJAppDelegate class]));
    }
}
