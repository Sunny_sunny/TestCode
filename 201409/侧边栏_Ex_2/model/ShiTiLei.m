//
//  ShiTiLei.m
//  Cell_ScrollVIew
//
//  Created by administrator on 14-9-1.
//  Copyright (c) 2014年 bwj. All rights reserved.
//

#import "ShiTiLei.h"

@implementation ShiTiLei
-(id)initWithDetail:(NSString *)detail ImageArray:(NSArray *)imageArray{
    if (self = [super init]) {
        self.detail = detail;
        self.imageArray = [NSMutableArray arrayWithArray:imageArray];
    }
    return self;
}

+(id)shiTiLeiWithDetail:(NSString *)detail ImageArray:(NSArray *)imageArray{
    return [[self alloc] initWithDetail:detail ImageArray:imageArray];
}
@end
