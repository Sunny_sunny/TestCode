//
//  House.m
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "House.h"

@implementation House

-(id)initWithHouseType:(int)houseType houseCity:(NSString *)houseCity houseStreet:(NSString *)houseStreet housePersons:(int)housePersons houseBedroom:(int)houseBedroom houseBed:(int)houseBed houseToilet:(int)houseToilet houseTitle:(NSString *)houseTitle housePrice:(int)housePrice houseNotice:(NSString *)houseNotice houseStatus:(int)houseStatus houseDate:(double)houseDate houseExpirationdate:(double)houseExpirationdate houseOwner:(int)houseOwner
{
    
    self = [super init];
    if (self) {
        self.houseCity = houseCity;
        self.houseCity = houseCity;
        self.houseStreet = houseStreet;
        self.housePersons = housePersons;
        self.houseBedroom = houseBedroom;
        self.houseBed = houseBed;
        self.houseToilet = houseToilet;
        self.houseTitle = houseTitle;
        self.housePrice = housePrice;
        self.houseNotice = houseNotice;
        self.houseStatus = houseStatus;
        self.houseDate = houseDate;
        self.houseExpirationdate = houseExpirationdate;
        self.houseOwner = houseOwner;
    }
    
    return self;
}

+(id)houseWithHouseType:(int)houseType houseCity:(NSString *)houseCity houseStreet:(NSString *)houseStreet housePersons:(int)housePersons houseBedroom:(int)houseBedroom houseBed:(int)houseBed houseToilet:(int)houseToilet houseTitle:(NSString *)houseTitle housePrice:(int)housePrice houseNotice:(NSString *)houseNotice houseStatus:(int)houseStatus houseDate:(double)houseDate houseExpirationdate:(double)houseExpirationdate houseOwner:(int)houseOwner
{
    
    return [[self alloc]initWithHouseType:houseType houseCity:houseCity houseStreet:houseStreet housePersons:housePersons houseBedroom:houseBedroom houseBed:houseBed houseToilet:houseToilet houseTitle:houseTitle housePrice:housePrice houseNotice:houseNotice houseStatus:houseStatus houseDate:houseDate houseExpirationdate:houseExpirationdate houseOwner:houseOwner];
}

@end
