//
//  WhyHireViewController.m
//  Volver
//
//  Created by admin on 14-9-25.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "WhyHireViewController.h"
#import "common.h"
#import "IntroControll.h"
#define kHeight 568
#define kWidth 320

@interface WhyHireViewController ()
{
    NSMutableArray *imageArray;
    UIPageControl *pageControl; 
    int index;
}

@end

@implementation WhyHireViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)init
{
    self = [super initWithNibName:nil bundle:nil];
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.modalPresentationStyle = UIModalPresentationFullScreen;
    return self;
}

- (void) loadView {
    [super loadView];
    IntroModel *model1 = [[IntroModel alloc] initWithTitle:@"结识世界各地的旅行者" description:@"成为房东，把世界带到您的家门口。" image:@"whyHire_01.jpg"];
    
    IntroModel *model2 = [[IntroModel alloc] initWithTitle:@"为您的下一次旅行赚取旅费" description:@"不仅可以帮补您的日常开支，还能为您赚取下一个假期的旅费。" image:@"whyHire_02_02.jpg"];
    
    IntroModel *model3 = [[IntroModel alloc] initWithTitle:@"改变世界" description:@"加入Volver，构建一个更加热情好客的世界。" image:@"whyHire_03_03.jpg"];
    IntroModel *model4 = [[IntroModel alloc] initWithTitle:@"尽情享受" description:@"带着ta，探索不一样的世界。" image:@"whyHire_04_04.jpg"];
    self.view = [[IntroControll alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height + 64) pages:@[model1, model2, model3,model4]];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    //添加返回按钮
    addBackButton();
    self.title = @"为什么要出租？";
    self.navigationItem.rightBarButtonItem.tintColor = kThemeColor;
   
}


#pragma mark 返回按钮事件
-(void)goBack:(id)sender{
    back;
}

@end
