//
//  VOLFavoriteCellModel.h
//  Volver
//
//  Created by administrator on 14-10-14.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VOLFavoriteCellModel : NSObject

@property int favoriteID;
@property int houseID;
@property (copy,nonatomic) NSString *houseImageName;
@property (copy,nonatomic) NSString *houseTitle;
@property (copy,nonatomic) NSString *houseType;
@property float housePrice;
@property (copy,nonatomic) NSString *houseAddress;
@property int houseOwner;
@property (assign, nonatomic) double houseExpirationdate;


@end
