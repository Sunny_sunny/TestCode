//
//  House.h
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface House : NSObject

@property (assign, nonatomic) int houseID;
@property (copy,nonatomic) NSString *houseNum;
@property (assign, nonatomic) int houseType;
@property (copy,nonatomic) NSString *houseProvince;
@property (copy, nonatomic) NSString *houseCity;
@property (copy, nonatomic) NSString *houseStreet;
@property (assign, nonatomic) float houseLongitude;
@property (assign, nonatomic) float houseLatitude;
@property (assign, nonatomic) int housePersons;
@property (assign, nonatomic) int houseBedroom;
@property (assign, nonatomic) int houseBed;
@property (assign, nonatomic) int houseToilet;
@property (copy, nonatomic) NSString *houseTitle;
@property (copy,nonatomic) NSString *houseDesc;
@property (assign, nonatomic) float housePrice;
@property (copy,nonatomic) NSString *houseNear;
@property (copy,nonatomic) NSString *houseTraffic;
@property (assign, nonatomic) NSString *houseNotice;
@property (assign, nonatomic) int houseStatus;
@property (assign, nonatomic) double houseDate;
@property (assign, nonatomic) double houseExpirationdate;
@property (assign, nonatomic) float houseScore;
@property (assign, nonatomic) int houseImages;
@property (assign, nonatomic) int houseUsing;
@property (assign, nonatomic) int houseFacility;
@property (assign, nonatomic) int houseOwner;

-(id)initWithHouseType:(int)houseType houseCity:(NSString *)houseCity houseStreet:(NSString *)houseStreet housePersons:(int)housePersons houseBedroom:(int)houseBedroom houseBed:(int)houseBed houseToilet:(int)houseToilet houseTitle:(NSString *)houseTitle housePrice:(int)housePrice houseNotice:(NSString *)houseNotice houseStatus:(int)houseStatus houseDate:(double)houseDate houseExpirationdate:(double)houseExpirationdate houseOwner:(int)houseOwner;
+(id)houseWithHouseType:(int)houseType houseCity:(NSString *)houseCity houseStreet:(NSString *)houseStreet housePersons:(int)housePersons houseBedroom:(int)houseBedroom houseBed:(int)houseBed houseToilet:(int)houseToilet houseTitle:(NSString *)houseTitle housePrice:(int)housePrice houseNotice:(NSString *)houseNotice houseStatus:(int)houseStatus houseDate:(double)houseDate houseExpirationdate:(double)houseExpirationdate houseOwner:(int)houseOwner;

@end
