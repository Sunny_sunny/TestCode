//
//  VOLFifthViewController.m
//  Volver
//
//  Created by administrator on 14-9-25.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLFifthViewController.h"
#import "NSString+ext.h"
#import "common.h"
#define FONT_SIZE 15.0f
@interface VOLFifthViewController (){
    NSMutableArray *array ;
    NSArray *imagearray ;
    UIFont *font ;
}
@end

@implementation VOLFifthViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"欢迎房客";
        addBackButton();
    self.tableview.dataSource = self;
    self.tableview.delegate = self ;
    
    //添加内容数组
    array = [NSMutableArray array];
    [array addObject:@"无论房客是乘坐飞机,火车还是汽车到达，为他们提供前往您房源的清晰路线都会让他们觉得非常贴心。"];
    
    [array addObject:@"顺利的入住让你们双方都留下良好的第一印象，把客人迎进家里，开始他们的历险旅程或在门口迎接房客"];
    
    [array addObject:@"带他们参观一下您的空间，告诉房客他们可以找到说明物品，以及在哪里可以找到所需要的物品"];
    
    //创建图片数组
    imagearray = [[NSArray alloc]initWithObjects:@"HosTraffics.png",@"check_0.png" ,@"Hosguides.png",nil];
    
    font = [UIFont fontWithName:@"Arial-BoldItalicMT" size:FONT_SIZE];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - 返回
-(void) goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 返回行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [array count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //声明cell和label
    UITableViewCell *cell;
    UILabel *label ;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithFrame:CGRectZero];
    }
    
    //在cell里添加照片
    NSString *imagename = [imagearray objectAtIndex:indexPath.row];
    UIImage *im = [UIImage imageNamed:imagename];
    UIImageView *image = [[UIImageView alloc]initWithImage:im];
    image.frame= CGRectMake(10, 0, 300, 130);
    [[cell contentView ]addSubview:image];
    
    label = [[UILabel alloc] initWithFrame:CGRectZero];
    //当一行的最后一个单词如果写在那行中的话会超过参数size的width,令这个单词是写在下一行
    [label setLineBreakMode:NSLineBreakByWordWrapping];
    //不限制label行数
    label.numberOfLines = 0 ;
    //设置字体
    [label setFont:font];
    label.textColor = [UIColor darkGrayColor];
    //读取label的值
    NSString *text = [array objectAtIndex:[indexPath row]];
    //求出text的高度
    CGSize size = [text calculateSize:CGSizeMake(300, FLT_MAX) font:font];
    //设置label的frmae
    label.frame = CGRectMake(10, 155, size.width, size.height);
    [label setText:text];
    //添加label
    [[cell contentView] addSubview:label];
    //去掉点击效果
    cell.selectionStyle = UITableViewCellSelectionStyleNone ;
    
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //运用拓展类里面的方法来计算text的高度
    NSString *text = [array objectAtIndex:indexPath.row];
    CGSize size = [text calculateSize:CGSizeMake(300, FLT_MAX) font:font];
    CGFloat height = size.height ;
    
    //返回值等于text的高度+图片的高度+图片与label中间的间隔+label与底部之间的间隔
    return height  + 190 ;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tableview deselectRowAtIndexPath:indexPath animated:YES];
}
@end
