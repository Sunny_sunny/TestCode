//
//  VOLHistoryCellModel.h
//  Volver
//
//  Created by administrator on 14-10-21.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VOLHistoryCellModel : NSObject

@property int orderFormID;
@property int houseID;
@property int isComment;
@property (copy,nonatomic) NSString *houseImageName;
@property (copy,nonatomic) NSString *houseTitle;
@property (copy,nonatomic) NSString *houseType;
@property float housePrice;


@end
