//
//  VOLSoonPublishViewController.h
//  Volver
//
//  Created by administrator on 14-9-16.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZYQAssetPickerController.h"

@interface VOLSoonPublishViewController : UIViewController<UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ZYQAssetPickerControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *addImageView;
@property (weak, nonatomic) IBOutlet UITableView *otherInfoTableVIew;
@property (weak, nonatomic) IBOutlet UIButton *publisButton;

- (IBAction)publish:(id)sender;

@end
