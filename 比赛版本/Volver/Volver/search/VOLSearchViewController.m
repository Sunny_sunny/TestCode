//
//  VOLSearchViewController.m
//  Volver
//
//  Created by administrator on 14-10-27.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLSearchViewController.h"

#import "common.h"
#import "HTTPDefine.h"
#import "VOLAppDelegate.h"

#import "REFrostedViewController.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "JSONKit.h"
#import "MJRefresh.h"

#import "VOLSelectionViewController.h"
#import "VOLSearchCleanButtonView.h"

@interface VOLSearchViewController ()
{
    VOLAppDelegate *del;
    NSFileManager *manager;
    
    UISearchBar *search;
    //单击手势
    UITapGestureRecognizer *tapGesture;
    //存放历史记录
    NSMutableArray *historyData;
    NSString *historyFilePath;
}
@end

@implementation VOLSearchViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    //初始化一些对象
    del = kGetDel;
    manager = [NSFileManager defaultManager];
    historyData = [NSMutableArray array];
    
    //添加菜单栏
    addLeftNavigationBarButton();
    
    //添加搜索栏到导航栏
    search = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 280, 60)];
    search.tintColor = kThemeColor;
    search.delegate = self;
    search.placeholder = @"请输入城市名";
    self.navigationItem.titleView = search;
    
    //添加footerView
    self.tableView.tableFooterView = [[UILabel alloc] init];
    
    //初始化单击手势
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHandle:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    
    //历史记录数组添加数组
    
    NSString *historyDirectory = [NSString stringWithFormat:@"%@historyCityName",kGetDocumentsPath];
    if (![manager fileExistsAtPath:historyDirectory]) {
        [manager createDirectoryAtPath:historyDirectory withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    historyFilePath = [NSString stringWithFormat:@"%@historyCityName/historyCityName.plist",kGetDocumentsPath];
    if ([manager fileExistsAtPath:historyFilePath]) {
        NSArray *array = [NSArray arrayWithContentsOfFile:historyFilePath];
        [historyData addObjectsFromArray:array];
    }else{
        [historyData addObject:@"当前位置"];
    }
    
    //添加清除
    NSArray *cleanBtnNib = [[NSBundle mainBundle] loadNibNamed:@"searchCleanButtonView" owner:self options:nil];
    VOLSearchCleanButtonView *cleanBtnView = cleanBtnNib[0];
    [cleanBtnView.cleanButton setTitleColor:kThemeColor forState:UIControlStateNormal];
    [cleanBtnView.cleanButton setTitleColor:kBlackColor forState:UIControlStateHighlighted];
    //添加动作
    [cleanBtnView.cleanButton addTarget:self action:@selector(cleanHistory:) forControlEvents:UIControlEventTouchUpInside];
    self.tableView.tableFooterView = cleanBtnView;
    self.tableView.tableFooterView.hidden = YES;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    if (historyData.count == 1) {
        self.tableView.tableFooterView.hidden = YES;
    }else{
        self.tableView.tableFooterView.hidden = NO;
    }
    [self.tableView reloadData];
}

#pragma mark - 按钮点击事件
#pragma mark 菜单栏
-(void)showMenu:(id)sender{
    kActionOfMenuButton;
}

#pragma mark 清除历史记录
-(void)cleanHistory:(UIButton *)sender{
    [historyData removeAllObjects];
    [historyData addObject:@"当前位置"];
    [historyData writeToFile:historyFilePath atomically:YES];
    [self.tableView reloadData];
    self.tableView.tableFooterView.hidden = YES;
}


#pragma mark - 手势动作
-(void)tapHandle:(UITapGestureRecognizer *)recognizer{
    [search resignFirstResponder];
    //移除手势
    [self.view removeGestureRecognizer:recognizer];
}


#pragma mark - 代理方法

#pragma mark - UISearchBar代理方法
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    //添加点击手势
    [self.view addGestureRecognizer:tapGesture];
    
    return YES;
}

#pragma mark 点击search按钮动作
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    //收回键盘，移除手势
    [searchBar resignFirstResponder];
    [self.view removeGestureRecognizer:tapGesture];
    //保存记录
    if (![historyData containsObject:searchBar.text]) {
        [historyData addObject:searchBar.text];
        [historyData writeToFile:historyFilePath atomically:YES];
    }
    
    //跳转界面
    VOLSelectionViewController *selectionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"selectionController"];
    selectionVC.cityName = searchBar.text;
    selectionVC.sourceFrom = 2;
    del.isRefresh = 6;
    [self.navigationController pushViewController:selectionVC animated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return historyData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    if (indexPath.row == 0) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }else{
        cell.textLabel.font = [UIFont fontWithName:@"System" size:15];
        cell.textLabel.textColor = kPriceLabelColor;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
    
    cell.textLabel.text = historyData[indexPath.row];
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    VOLSelectionViewController *selectionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"selectionController"];
    selectionVC.cityName = historyData[indexPath.row];
    if (indexPath.row == 0) {
        selectionVC.sourceFrom = 1;
    }else{
        selectionVC.sourceFrom = 2;
    }
    
    del.isRefresh = 6;
    [self.navigationController pushViewController:selectionVC animated:YES];
}



@end
