//
//  VOLSearchViewController.h
//  Volver
//
//  Created by administrator on 14-10-27.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VOLSearchViewController : UITableViewController<UISearchBarDelegate>

@end
