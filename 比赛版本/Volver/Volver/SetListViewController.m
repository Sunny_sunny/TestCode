//
//  SetListViewController.m
//  Volver
//
//  Created by admin on 14-9-25.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "SetListViewController.h"
#import "common.h"

#import "UIViewController+REFrostedViewController.h"
#import "REFrostedViewController.h"
#import "REFrostedContainerViewController.h"
#import "WhyHireViewController.h"
#import "VolverHelpViewController.h"
#import "VOLAppDelegate.h"
#import "MBProgressHUD.h"
#import "HowUseViewController.h"
#import "PersonDataViewController.h"

#import "VOLSelectionViewController.h"

//拉一个控件
@interface SetListViewController ()<MBProgressHUDDelegate>
{
    NSMutableArray *listArray;
    MBProgressHUD *HUD;
    VOLAppDelegate *del;
}

@end

@implementation SetListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"账号";
    //添加返回、注销按钮并设置颜色
    addLeftNavigationBarButton();
    
    //初始化del
    del = kGetDel;
    
    //添加“退出”按钮
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"退出" style:UIBarButtonItemStylePlain target:self action:@selector(userLogout:)];
    [self.navigationItem.rightBarButtonItem setTintColor:kThemeColor];
    listArray = [NSMutableArray arrayWithObjects:@"个人资料",@"如何使用",@"为什么要出租？",@"Volver帮助",@"清除缓存",@"喜欢Volver？",@"当前版本",nil];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.tableView.tableFooterView = [[UILabel alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}
#pragma mark - 按钮点击事件

#pragma mark 注销并提示用户
-(void)userLogout:(id)sender{
    
    del.isLogin = NO;
    del.currentUser = nil;
    del.currentUser = [[User alloc] init];
    //取消自动登录
    del.isAutologin = 0;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:[NSString stringWithFormat:@"%d",del.isAutologin] forKey:@"isAutoLogin"];

    //提醒用户
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"已退出当前用户" delegate:self cancelButtonTitle:@"好的，谢谢！" otherButtonTitles: nil];
    [alert show];
    //聊天服务器退出
    XMPPPresence *presence = [XMPPPresence presenceWithType:@"unavailable"];
    [del.xmppsteam sendElement:presence];
    
    [del.xmppsteam disconnect];
    [del.rosters removeAllObjects];
}

#pragma mark 菜单按钮的动作
- (void)showMenu:(id)sender {
    kActionOfMenuButton;
}


#pragma mark - 代理方法
#pragma mark - tableView代理方法
#pragma mark 返回分组数
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

#pragma mark 返回每组行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [listArray count];
}

#pragma mark 返回每行的单元格
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([indexPath row] == 6) {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSDictionary *dic = [[NSBundle mainBundle] infoDictionary];
        NSString *versionString = [dic objectForKey:@"CFBundleVersion"];
        cell.textLabel.text = listArray[indexPath.row];
        cell.detailTextLabel.text = versionString;
        return cell;
    }else{
        UITableViewCell *cell = [[UITableViewCell alloc]init];
        cell.textLabel.text = [listArray objectAtIndex:[indexPath row]];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //取消被点击时的背景色
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch ([indexPath row]) {
        case 0:
        {
            //跳转到个人资料界面
            PersonDataViewController *person = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonDataViewController"];
            person.sourceFrom = 0;
            [self.navigationController pushViewController:person animated:YES];
            
            
        }
            break;
        case 1:
        {
            //跳转到如何使用界面
            HowUseViewController *howUse = [self.storyboard instantiateViewControllerWithIdentifier:@"HowUseViewController"];
            [self.navigationController pushViewController:howUse animated:YES];
        }
            break;
        case 2:
        {
            //跳转到为什么要出租界面
            WhyHireViewController *whyHireView = [self.storyboard instantiateViewControllerWithIdentifier:@"whyHireView"];
            [self.navigationController pushViewController:whyHireView animated:YES];
        }
            break;
        case 3:
        {
            //跳转到Volver帮助界面
            VolverHelpViewController *volverHelpView = [self.storyboard instantiateViewControllerWithIdentifier:@"volverHelpView"];
            [self.navigationController pushViewController:volverHelpView animated:YES];
        }
            break;
        case 4:
        {
            //跳转到清除缓存界面
            HUD = [[MBProgressHUD alloc]initWithView:self.navigationController.view];
            [self.navigationController.view addSubview:HUD];

            __block BOOL isClean = NO;
            [HUD showAnimated:YES whileExecutingBlock:^{
                //消除图片操作
                isClean = [self cleanAllImages:del.currentUser.userID];
            } completionBlock:^{
                [HUD removeFromSuperview];
                HUD = nil;
                
                HUD = [[MBProgressHUD alloc] initWithView:self.view];
                [self.view addSubview:HUD];
                
                if (isClean) {
                    HUD.labelText = @"清除成功";
                    HUD.mode = MBProgressHUDModeCustomView;
                    HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                    [HUD showAnimated:YES whileExecutingBlock:^{
                        sleep(1);
                    } completionBlock:^{
                        [HUD removeFromSuperview];
                        HUD = nil;
                    }];
                }
            }];
        }
            break;
        case 5:
        {
            //跳转到APP下载界面
            NSString *appURL = [NSString stringWithFormat:@"http://fir.im/volver"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appURL]];
        }
            break;
            //        case 6:
            //        {
            //            NSLog(@"意见反馈");
            //
            //        }
            break;
        default:
            break;
    }
}

- (void)myProgressTask {
	// This just increases the progress indicator in a loop
	float progress = 0.0f;
	while (progress < 1.0f) {
		progress += 0.01f;
		HUD.progress = progress;
		usleep(40000);
	}
}

-(void)switchChange:(UISwitch *)switchChange
{
}

#pragma mark - alert代理方法
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    //更改del.lastIndex为精选界面的indexPath
    NSIndexPath *selectionIndex = [NSIndexPath indexPathForRow:1 inSection:0];
    del.lastIndex = selectionIndex;
    
    //跳转到精选界面
    UINavigationController *navigationController = (UINavigationController *)self.frostedViewController.contentViewController;
    
    VOLSelectionViewController *selectionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"selectionController"];
    selectionVC.sourceFrom = 0;
    del.isRefresh = 6;
    navigationController.viewControllers = @[selectionVC];
}

#pragma mark - 清除图片操作
-(BOOL)cleanAllImages:(int)userID{
    NSString *imageDocumentPath = [NSString stringWithFormat:@"%@%d",kGetDocumentsPath,userID];
    NSFileManager *manager = [NSFileManager defaultManager];
    return [manager removeItemAtPath:imageDocumentPath error:nil];
}
@end



