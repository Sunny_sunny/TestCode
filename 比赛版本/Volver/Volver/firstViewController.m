//
//  firstViewController.m
//  haokezhidao
//
//  Created by administrator on 14-9-18.
//  Copyright (c) 2014年 administrator. All rights reserved.
//

#import "firstViewController.h"
#import "NSString+ext.h"
#import "common.h"
#define FONT_SIZE 15.0f
@interface firstViewController ()
{
    NSMutableArray *array ;
    NSArray *imagearray ;
    UIFont *font ;
}
@end

@implementation firstViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"沟通";
    
    self.tableview.dataSource = self;
    self.tableview.delegate = self ;
    
    //添加内容数组
    array = [NSMutableArray array];
//    [array addObject:@"写信,打电话甚至派信鸽都可以，及时回复房客的咨询能改善每个人的体验。经统计，12小时内回复的房东每个月收到的预定是其他房东的两倍."];
    [array addObject:@"写信,打电话甚至派信鸽都可以."];
    [array addObject:@"为他们联系您的瑜伽导师,让他们在旅途中拥有灵活的身体您完全可以为他们的旅程留下难忘的回忆."];
    [array addObject:@"如果您的卧室添加了新家具，您更改了退房时间，或者收养了四条腿的新朋友,请告知您的房客"];
    
    //创建图片数组
    imagearray = [[NSArray alloc]initWithObjects:@"Hosresponds.png",@"Hosindividuations.png" ,@"Hosupdates.png",nil];
    //设置字体
    font = [UIFont fontWithName:@"Arial-BoldItalicMT" size:FONT_SIZE];
    //返回
   addBackButton();
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 返回
-(void) goBack:(id)sender{
    back;
}

#pragma mark - 返回行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [array count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //声明cell和label
    UITableViewCell *cell;
    UILabel *label ;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]init];
    }
    
    //在cell里添加照片
    NSString *imagename = [imagearray objectAtIndex:indexPath.row];
    UIImage *im = [UIImage imageNamed:imagename];
    UIImageView *image = [[UIImageView alloc]initWithImage:im];
    image.frame= CGRectMake(10, 0, 300, 130);
    [[cell contentView ]addSubview:image];
    
    label = [[UILabel alloc] initWithFrame:CGRectZero];
    //当一行的最后一个单词如果写在那行中的话会超过参数size的width,令这个单词是写在下一行
    [label setLineBreakMode:NSLineBreakByWordWrapping];
    //不限制label行数
    label.numberOfLines = 0 ;
    //设置字体
    [label setFont:font];
    label.textColor = [UIColor darkGrayColor];
    //读取label的值
    NSString *text = [array objectAtIndex:[indexPath row]];
    //求出text的高度
    CGSize size = [text calculateSize:CGSizeMake(300, FLT_MAX) font:font];
    //设置label的frmae
    label.frame = CGRectMake(10, 155, size.width, size.height);
    [label setText:text];
    //添加label
    [[cell contentView] addSubview:label];
    //去掉点击效果
    cell.selectionStyle = UITableViewCellSelectionStyleNone ;
    
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //运用拓展类里面的方法来计算text的高度
    NSString *text = [array objectAtIndex:indexPath.row];
    CGSize size = [text calculateSize:CGSizeMake(300, FLT_MAX) font:font];
    CGFloat height = size.height ;
    
    //返回值等于text的高度+图片的高度+图片与label中间的间隔+label与底部之间的间隔
    return height  + 190 ;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tableview deselectRowAtIndexPath:indexPath animated:YES];
}
@end
