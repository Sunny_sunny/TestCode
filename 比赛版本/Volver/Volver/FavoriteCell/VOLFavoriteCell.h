//
//  VOLFavoriteCell.h
//  Volver
//
//  Created by administrator on 14-10-14.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VOLFavoriteCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *houseImage;
@property (weak, nonatomic) IBOutlet UILabel *houseTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *houseTypeAndPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *houseAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *cellNoticeLabel;


@end
