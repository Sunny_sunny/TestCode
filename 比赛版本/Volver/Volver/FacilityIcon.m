//
//  FacilityIcon.m
//  ScrollViewTest
//
//  Created by administrator on 14-9-18.
//  Copyright (c) 2014年 bwj. All rights reserved.
//

#import "FacilityIcon.h"

@implementation FacilityIcon
-(id)initWithImageName:(NSString *)imageName labelText:(NSString *)labelText{
    if (self = [super init]) {
        self.imageName = imageName;
        self.labelText = labelText;
    }
    return self;
}
+(id)facilityIconWithImageName:(NSString *)imageName labelText:(NSString *)labelText{
    return [[self alloc] initWithImageName:imageName labelText:labelText];
}

@end
