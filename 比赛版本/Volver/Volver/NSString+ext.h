//
//  NSString+ext.h
//  haoke0925
//
//  Created by administrator on 14-9-25.
//  Copyright (c) 2014年 administrator. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ext)
- (CGSize)calculateSize:(CGSize)size font:(UIFont *)font;
@end
