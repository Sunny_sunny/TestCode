//
//  VOLMyHouseCell.h
//  Volver
//
//  Created by administrator on 14-9-29.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VOLMyHouseCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *houseImageView;
@property (weak, nonatomic) IBOutlet UILabel *houseTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *houseTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *houseAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *cellNoticeLabel;

@end
