//
//  VOLhouseExpirationdateViewController.m
//  Volver
//
//  Created by administrator on 14-9-23.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLhouseExpirationdateViewController.h"
#import "VOLAppDelegate.h"
#import "common.h"
@interface VOLhouseExpirationdateViewController ()
{
    VOLAppDelegate *del ;
}
@end

@implementation VOLhouseExpirationdateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    del  = [[UIApplication sharedApplication]delegate];
    
    addBackButton();
    
    self.title = @"编辑截止日期" ;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(EditExpirationdatefinish:)];
    self.navigationItem.rightBarButtonItem.tintColor = kThemeColor ;
    self.datepicker.datePickerMode = UIDatePickerModeDate ;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)EditExpirationdatefinish:(id)sender{
    
    //获取当前时间和datepicker选择的时间
    NSDate *date = self.datepicker.date;
    NSDate *nowdate = [NSDate date];
    
    //转换成秒
    NSTimeInterval k1 = [date timeIntervalSince1970];
    NSTimeInterval k2 = [nowdate timeIntervalSince1970];
    
    //如果选择的日期已经过去，则重新选择
    if (k1 >= k2) {
        
    del.publishHouse.houseExpirationdate = k1;
    [self.navigationController popViewControllerAnimated:YES];
        
    }
    else {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"不能选择已经过去的日期" delegate:self cancelButtonTitle:@"重新选择日期" otherButtonTitles: nil];
        [self.view addSubview:alert];
        [alert show];
    }
}
@end
