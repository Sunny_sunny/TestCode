//
//  VOLSummaryViewController.h
//  Volver
//
//  Created by administrator on 14-9-19.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VOLSummaryViewController : UIViewController<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *summarytextview;

@end
