//
//  VOLPriceViewController.m
//  Volver
//
//  Created by administrator on 14-9-17.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLPriceViewController.h"
#import "common.h"
#import "VOLAppDelegate.h"
#import "VOLSoonPublishViewController.h"
@interface VOLPriceViewController ()
{
    VOLAppDelegate *del ;
}
@end

@implementation VOLPriceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    del = [[UIApplication sharedApplication]delegate ];
    
    self.title = @"价格";
    
    addBackButton();
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(finish1:)];
    self.navigationItem.rightBarButtonItem.tintColor = kThemeColor;
    
//    填写以存在的price的值
    if (del.publishHouse.housePrice) {
        self.priceLabel.text =[NSString stringWithFormat:@"%.1f" ,del.publishHouse.housePrice];
    }
    
    //调用数字键盘
    self.priceLabel.keyboardType = UIKeyboardTypeDecimalPad ;
    [UIView animateWithDuration:0.5 animations:^{
        [self.priceLabel becomeFirstResponder];
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark 返回方法
-(void)goBack:(id)sender{
    back;
}

-(void)finish1:(id)sender{
    del.publishHouse.housePrice = [self.priceLabel.text floatValue];
    back;
}


@end
