//
//  VOLSummaryViewController.m
//  Volver
//
//  Created by administrator on 14-9-19.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLSummaryViewController.h"
#import "common.h"
#import "VOLAppDelegate.h"
@interface VOLSummaryViewController ()
{
    VOLAppDelegate *del ;
}
@end

@implementation VOLSummaryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    del  = [[UIApplication sharedApplication]delegate];
    
    addBackButton();
    
    self.title = @"编辑摘要" ;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(descfinish:)];
    self.navigationItem.rightBarButtonItem.tintColor = kThemeColor ;
    
    if (del.publishHouse.houseDesc) {
        self.summarytextview.text = del.publishHouse.houseDesc ;
    }
    
    //获取焦点
    [UIView animateWithDuration:0.5 animations:^{
        [self.summarytextview becomeFirstResponder];
    }];
    
    
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.summarytextview.layer.masksToBounds = YES;
    self.summarytextview.layer.cornerRadius = 10.0f;
    self.summarytextview.frame = CGRectMake(5, 70, 310, 250);

}

#pragma mark 返回方法
-(void)goBack:(id)sender{
    back;
}

-(void)descfinish:(id)sender{
    del.publishHouse.houseDesc = self.summarytextview.text ;
    
    back;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//更改UITextView的光标的位置：
//- (void)textViewDidChangeSelection:(UITextView *)textView
//{
//    NSRange range;
//    range.location = 0;
//    range.length = 0;
//    textView.selectedRange = range;
//}

@end
