//
//  VOLSelectInfViewController.m
//  Volver
//
//  Created by administrator on 14-9-17.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLSelectInfViewController.h"
#import "common.h"
#import "VOLAppDelegate.h"
@interface VOLSelectInfViewController ()
{
    VOLAppDelegate *del ;
    NSDictionary *dic ;
    NSArray *facilityarray ;
    NSArray *array ;
    int i;
    int a0,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11 ;
    CGFloat height;
    CGFloat width ;
}
@end

@implementation VOLSelectInfViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    del = kGetDel;
    
//    从plis文件读取值
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"houseFacility" ofType:@".plist"];
    

    dic = [NSDictionary dictionaryWithContentsOfFile:filePath];
//    NSLog(@"%@",dic);
    facilityarray = [dic allKeys];
    
    self.trafficTextView.delegate = self;
    self.FaciitytextView.delegate = self;
    self.trafficTextView.tag = 2 ;
    self.FaciitytextView.tag = 1 ;
//    初始化标志数字
    i = 0 ;
    a0 = del.publishFacility.hotwater ;
    a1 = del.publishFacility.elevator ;
    a2 = del.publishFacility.bathroom ;
    a3 = del.publishFacility.kitchen  ;
    a4 = del.publishFacility.aircondition ;
    a5 = del.publishFacility.isSmoke ;
    a6 = del.publishFacility.shampoo ,
    a7 = del.publishFacility.isPat;
    a8 = del.publishFacility.wifi ;
    a9 = del.publishFacility.tv ;
    a10 = del.publishFacility.parking ;
    a11 = del.publishFacility.barrierFree;
    
    self.tableview.dataSource = self ;
    self.tableview.delegate = self ;
    
//    导航信息
    self.title = @"可选信息";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(finish:)];
    
    self.navigationItem.rightBarButtonItem.tintColor = kThemeColor ;
    addBackButton();
    
    [self.segement addTarget:self action:@selector(changeinfor:) forControlEvents:UIControlEventValueChanged];
    self.FaciitytextView.hidden = YES ;
    self.facilitylabel.hidden = YES ;
    self.trafficTextView.hidden = YES ;
    self.trafficLabel.hidden = YES ;
    
//    判断读取以存储的信息
    if (del.publishHouse.houseTraffic) {
        self.trafficTextView.text = del.publishHouse.houseTraffic;
    }
    if (del.publishHouse.houseNear) {
        self.FaciitytextView.text  = del.publishHouse.houseNear ;
    }
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    //设置成NO表示当前控件响应后会传播到其他控件上，默认为YES。
    tapGestureRecognizer.cancelsTouchesInView = NO;
    //将触摸事件添加到当前view
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
    //设置输入框为圆角
    self.FaciitytextView.layer.masksToBounds = YES;
    self.FaciitytextView.layer.cornerRadius = 5.0f;
    
    self.trafficTextView.layer.masksToBounds = YES;
    self.trafficTextView.layer.cornerRadius = 5.0f;
}
-(void)goBack:(id)sender{
    back;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [facilityarray count] ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]init];
    }
//    在默认的tableviewcell中添加自定义的imageview和label
    NSString *imagename = [dic valueForKey:[facilityarray objectAtIndex:indexPath.row]];
    UIImage *im = [UIImage imageNamed:imagename];
    UIImageView *image = [[UIImageView alloc]initWithImage:im];
    image.frame= CGRectMake(40, 8, 30, 30);
    
    CGRect rect = CGRectMake(100, 5, 100, 40);
    UILabel *label = [[UILabel alloc]initWithFrame:rect];
    label.text = [facilityarray objectAtIndex:indexPath.row];
    
    [[cell contentView] addSubview:label];
    [[cell contentView ]addSubview:image];

//    根据属性的值来改变字体的颜色
    switch (indexPath.row) {
        case 0:
           
            if (del.publishFacility.hotwater == 1) {
                label.text = [facilityarray objectAtIndex:indexPath.row];
                label.textColor = [UIColor redColor];
                cell.accessoryType =  UITableViewCellAccessoryCheckmark ;
            }
            else{
                cell.textLabel.textColor = [UIColor blackColor];
                cell.accessoryType = UITableViewCellAccessoryNone ;
            }
            break;
        case 1 :
            
            if (del.publishFacility.elevator == 1) {
                label.textColor = [UIColor redColor];
                cell.accessoryType =  UITableViewCellAccessoryCheckmark ;
            }
            else{
                label.text = [facilityarray objectAtIndex:indexPath.row];
                cell.textLabel.textColor = [UIColor blackColor];
                cell.accessoryType = UITableViewCellAccessoryNone ;
            }
            break;
        case 2 :
            if (del.publishFacility.bathroom == 1) {
                label.text = [facilityarray objectAtIndex:indexPath.row];
                label.textColor = [UIColor redColor];
                cell.accessoryType =  UITableViewCellAccessoryCheckmark ;
            }
            else{
                cell.textLabel.textColor = [UIColor blackColor];
                cell.accessoryType = UITableViewCellAccessoryNone ;
            }
                break;
            
        case 3 :
            
            if (del.publishFacility.kitchen == 1) {
                label.textColor = [UIColor redColor];
                cell.accessoryType =  UITableViewCellAccessoryCheckmark ;
            }
            else{
        label.textColor = [UIColor blackColor];
                cell.accessoryType = UITableViewCellAccessoryNone ;
            }
            break;
        case 4 :
            
            if (del.publishFacility.aircondition == 1) {
                label.textColor = [UIColor redColor];
                cell.accessoryType =  UITableViewCellAccessoryCheckmark ;
            }
            else{
                label.textColor = [UIColor blackColor];
                cell.accessoryType = UITableViewCellAccessoryNone ;
            }
            break;
        case 5 :
            
            if (del.publishFacility.isSmoke == 1) {
                label.textColor = [UIColor redColor];
                cell.accessoryType =  UITableViewCellAccessoryCheckmark ;
            }
            else{
                label.textColor = [UIColor blackColor];
                cell.accessoryType = UITableViewCellAccessoryNone ;
            }
            break;
        case 6 :
            
            if (del.publishFacility.shampoo == 1) {
               label.textColor = [UIColor redColor];
                cell.accessoryType =  UITableViewCellAccessoryCheckmark ;
            }
            else{
                label.textColor = [UIColor blackColor];
                cell.accessoryType = UITableViewCellAccessoryNone ;
            }
            break;
        case 7 :
            
            if (del.publishFacility.isPat == 1) {
               label.textColor = [UIColor redColor];
                cell.accessoryType =  UITableViewCellAccessoryCheckmark ;
            }
            else{
                label.textColor = [UIColor blackColor];
                cell.accessoryType = UITableViewCellAccessoryNone ;
            }
            break;
        case 8 :
            
            if (del.publishFacility.wifi == 1) {
                label.textColor = [UIColor redColor];
                cell.accessoryType =  UITableViewCellAccessoryCheckmark ;
            }
            else{
               label.textColor = [UIColor blackColor];
                cell.accessoryType = UITableViewCellAccessoryNone ;
            }
            break;
        case 9 :
            
            if (del.publishFacility.tv == 1) {
                label.textColor = [UIColor redColor];
                cell.accessoryType =  UITableViewCellAccessoryCheckmark ;
            }
            else{
                label.textColor = [UIColor blackColor];
                cell.accessoryType = UITableViewCellAccessoryNone ;
            }
            break;
        case 10 :
            
            if (del.publishFacility.parking == 1) {
                label.textColor = [UIColor redColor];
                cell.accessoryType =  UITableViewCellAccessoryCheckmark ;
            }
            else{
                label.textColor = [UIColor blackColor];
                cell.accessoryType = UITableViewCellAccessoryNone ;
            }
            break;
        case 11 :
            
            if (del.publishFacility.barrierFree == 1) {
                label.textColor = [UIColor redColor];
                cell.accessoryType =  UITableViewCellAccessoryCheckmark ;
            }
            else{
                label.textColor = [UIColor blackColor];
                cell.accessoryType = UITableViewCellAccessoryNone ;
            }
            break;
        
   }
    
    
    return cell ;

}

#pragma mark 通过标志数字的值来改变设施的属性值
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0 :
            if ( a0 ==  0) {
                del.publishFacility.hotwater = 1 ;
                a0 = 1 ;
                del.facilityCount++;
            }
            else{
                del.publishFacility.hotwater = 0 ;
                a0 = 0  ;
                del.facilityCount--;
            }
    
            break;
        case 1 :
            if ( a1 == 0) {
                del.publishFacility.elevator = 1 ;
                a1 = 1;
                del.facilityCount++;
            }
            else{
                del.publishFacility.elevator = 0 ;
                a1 = 0;
                del.facilityCount--;
            }
    
            break;
        case 2:
            if (a2 == 0) {
                del.publishFacility.bathroom = 1 ;
                a2 = 1 ;
                del.facilityCount++;
            }
        else{
                del.publishFacility.bathroom = 0 ;
                a2 = 0 ;
                del.facilityCount--;
        }
            break;
        case 3:
            if (a3 == 0) {
                del.publishFacility.kitchen = 1 ;
                a3 = 1 ;
                del.facilityCount++;
            }
            else{
                del.publishFacility.kitchen = 0 ;
                a3 = 0 ;
                del.facilityCount--;
            }
            break;
        case 4:
            if (a4 == 0) {
                del.publishFacility.aircondition = 1 ;
                a4 = 1 ;
                del.facilityCount++;
            }
            else{
                del.publishFacility.aircondition = 0 ;
                a4 = 0 ;
                del.facilityCount--;
            }
            break;
        case 5:
            if (a5 == 0) {
                del.publishFacility.isSmoke = 1 ;
                a5 = 1 ;
                del.facilityCount++;
            }
            else{
                del.publishFacility.isSmoke = 0 ;
                a5 = 0 ;
                del.facilityCount--;
            }
            break;
        case 6:
            if (a6 == 0) {
                del.publishFacility.shampoo = 1 ;
                a6 = 1 ;
                del.facilityCount++;
            }
            else{
                del.publishFacility.shampoo = 0 ;
                a6 = 0 ;
                del.facilityCount--;
            }
            break;
        case 7:
            if (a7 == 0) {
                del.publishFacility.isPat = 1 ;
                a7 = 1 ;
                del.facilityCount++;
            }
            else{
                del.publishFacility.isPat = 0 ;
                a7 = 0 ;
                del.facilityCount--;
            }
            break;
        case 8:
            if (a8 == 0) {
                del.publishFacility.wifi = 1 ;
                a8 = 1;
                del.facilityCount++;
            }
            else{
                del.publishFacility.wifi = 0 ;
                a8 = 0 ;
                del.facilityCount--;
            }
            break;
        case 9:
            if (a9  == 0) {
                del.publishFacility.tv = 1 ;
                a9 = 1 ;
                del.facilityCount++;
            }
            else{
                del.publishFacility.tv = 0 ;
                a9 = 0;
                del.facilityCount--;
            }
            break;
        case 10:
            if (a10 == 0) {
                del.publishFacility.parking = 1 ;
                a10 = 1 ;
                del.facilityCount++;
            }
            else{
                del.publishFacility.parking = 0 ;
                a10 = 0;
                del.facilityCount--;
            }
            break;
        case 11:
            if (a11 == 0) {
                del.publishFacility.barrierFree = 1 ;
                a11 = 1;
                del.facilityCount++;
            }
            else{
                del.publishFacility.barrierFree = 0 ;
                a11 = 0 ;
                del.facilityCount--;
            }
            break;
    }
    [self.tableview reloadData] ;
}
-(void)finish:(id)sender{
    del.publishHouse.houseTraffic = self.trafficTextView.text;
    del.publishHouse.houseNear = self.FaciitytextView.text ;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 通过segement的只改变控件的显和隐

-(void)changeinfor:(id)sender{
    UISegmentedControl *se = sender ;
    if (se.selectedSegmentIndex == 0) {
        self.tableview.hidden = NO ;
        self.FaciitytextView.hidden = YES ;
        self.facilitylabel.hidden = YES ;
        self.trafficTextView.hidden = YES ;
        self.trafficLabel.hidden = YES ;
    }
    else{
        self.FaciitytextView.hidden = NO ;
        self.facilitylabel.hidden = NO ;
        self.trafficTextView.hidden = NO ;
        self.trafficLabel.hidden = NO ;
        self.tableview.hidden = YES ;
    }
}

#pragma mark - 键盘遮挡时向上移
-(void)textViewDidBeginEditing:(UITextView *)textView{

    if (self.view.bounds.size.height - textView.frame.origin.y -textView.frame.size.height < 216) {
        [UIView animateWithDuration:0.4 animations:^{
            
            height = self.view.frame.size.height;
        width = self.view.frame.size.width ;
            self.view.frame = CGRectMake(0, -100, width, height);
            
        }];
    }
   
}


//-(void)textViewDidEndEditing:(UITextView *)textView{
//    
//
//[self.view endEditing:YES];
//
//}

#pragma mark -点击return换行
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
   if (textView.tag == 1 && [text isEqualToString:@"\n"]) {
       [self.view endEditing:YES];
    }
    else if(textView.tag == 2 && [text isEqualToString:@"\n"]){
        [self.trafficTextView resignFirstResponder];
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionLayoutSubviews animations:^{
            self.view.frame = CGRectMake(0, 0, width, height);
        } completion:^(BOOL finished) {
            NSLog(@"向上移动·");
        }];
      
        
    }
    return YES ;
}

#pragma mark 还原输入框位置
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.4 animations:^{
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }];
    return YES;
}

-(void)keyboardHide:(UITapGestureRecognizer*)tap{
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.4 animations:^{
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }];
}
@end
