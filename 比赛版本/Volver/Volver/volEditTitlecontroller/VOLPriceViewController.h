//
//  VOLPriceViewController.h
//  Volver
//
//  Created by administrator on 14-9-17.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol priceViewdelegate<NSObject>
-(void) didFinishEditPrice;
@end
@interface VOLPriceViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *priceLabel;
@property (assign,nonatomic) id<priceViewdelegate>delegate ;

@end
