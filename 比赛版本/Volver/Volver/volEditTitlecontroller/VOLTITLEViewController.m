//
//  VOLTITLEViewController.m
//  Volver
//
//  Created by administrator on 14-9-18.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLTITLEViewController.h"
#import "common.h"
#import "VOLAppDelegate.h"
@interface VOLTITLEViewController ()
{
    VOLAppDelegate *del ;
}
@end

@implementation VOLTITLEViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    del = [[UIApplication sharedApplication]delegate] ;
    
    if (del.publishHouse.houseTitle) {
        
        self.TitleView.text = del.publishHouse.houseTitle ;
    }
    
    self.title = @"标题";
    
    addBackButton();
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(titlefinish:)];
    self.navigationItem.rightBarButtonItem.tintColor = kThemeColor;
    
    //获取焦点
    [UIView animateWithDuration:0.5 animations:^{
        [self.TitleView becomeFirstResponder];
    }];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.TitleView.layer.masksToBounds = YES;
    self.TitleView.layer.cornerRadius = 10.0f;
    self.TitleView.frame = CGRectMake(10, 70, 300, 150);
}
#pragma mark 返回方法
-(void)goBack:(id)sender{
    back;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)titlefinish:(id)sender{
    
    del.publishHouse.houseTitle = self.TitleView.text ;
    [self.navigationController popViewControllerAnimated:YES];

}
@end
