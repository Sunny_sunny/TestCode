//
//  VOLHouseTypeViewController.m
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLHouseTypeViewController.h"
#import "common.h"
#import "REFrostedViewController.h"
#import "VOLAppDelegate.h"

#import "VOLHouseType.h"
#import "VOLHouseTypeCell.h"

@interface VOLHouseTypeViewController ()
{
    NSArray *houseTypeSource;
}

@end

@implementation VOLHouseTypeViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"空间类型";
    
    //设置返回按钮,并实现goBack:方法
    addBackButton();
    
    //禁止滚动
    self.tableView.scrollEnabled = NO;
    //设置提示label
    UILabel *noticeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kViewWidth, kViewHeight -63 - 3 * 60)];
    noticeLabel.textAlignment = NSTextAlignmentCenter;
    noticeLabel.text = @"您想发布什么类型的空间";
    noticeLabel.alpha = 0.8;
    self.tableView.tableHeaderView = noticeLabel;
    
    //初始化数组内容
    VOLHouseType *wholeHouse = [VOLHouseType houseTypeWithIconImage:@"house.png" type:@"整套房子" typeDesc:@"您的整套房子"];
    VOLHouseType *singleRoom = [VOLHouseType houseTypeWithIconImage:@"singleRoom.png" type:@"独立房间" typeDesc:@"您房子里的一个单人房间"];
    VOLHouseType *uniteRoom = [VOLHouseType houseTypeWithIconImage:@"uniteRoom.png" type:@"合租房间" typeDesc:@"您房间里的一个床位"];
    
    houseTypeSource = [NSArray arrayWithObjects:wholeHouse,singleRoom,uniteRoom, nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark 返回按钮事件
-(void)goBack:(id)sender{
    back;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return houseTypeSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"houseTypeCell";
    VOLHouseTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    VOLHouseType *temp = houseTypeSource[indexPath.row];
    cell.typeImageView.image = [UIImage imageNamed:temp.iconImage];
    cell.typeLabel.text = temp.type;
    cell.typedescLabel.text = temp.typeDesc;
    
    return cell;
}

#pragma mark - Table View Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    VOLAppDelegate *del = kGetDel;
    switch (indexPath.row) {
        case 0:
            del.publishHouse.houseType = 0;
            break;
        case 1:
            del.publishHouse.houseType = 1;
            break;
        case 2:
            del.publishHouse.houseType = 2;
            break;
    }
}

@end




