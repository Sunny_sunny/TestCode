//
//  VOLBookHouseViewController.h
//  Volver
//
//  Created by administrator on 14-10-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "House.h"

@interface VOLBookHouseViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (retain, nonatomic)House *house;

@property (weak, nonatomic) IBOutlet UITableView *dateTableView;


@property (weak, nonatomic) IBOutlet UILabel *arriveLabel;
@property (weak, nonatomic) IBOutlet UILabel *leaveLabel;

@property (weak, nonatomic) IBOutlet UIButton *arriveButton;
@property (weak, nonatomic) IBOutlet UIButton *leaveButton;
@property (weak, nonatomic) IBOutlet UILabel *daysLabel;

@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIButton *subButton;
@property (weak, nonatomic) IBOutlet UITextField *personNumField;


@property (weak, nonatomic) IBOutlet UIView *priceView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;

@property (weak, nonatomic) IBOutlet UILabel *noticeLable;

- (IBAction)chooseArriveDate:(UIButton *)sender;
- (IBAction)chooseLeaveDate:(UIButton *)sender;

- (IBAction)changePersonNum:(UIButton *)sender;


@end
