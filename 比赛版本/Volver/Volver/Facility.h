//
//  Facility.h
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Facility : NSObject

@property (assign, nonatomic) int facilityID;
@property (assign, nonatomic) int houseID;
@property (assign, nonatomic) int shampoo;
@property (assign, nonatomic) int aircondition;
@property (assign, nonatomic) int kitchen;
@property (assign, nonatomic) int wifi;
@property (assign, nonatomic) int hotwater;
@property (assign, nonatomic) int parking;
@property (assign, nonatomic) int elevator;
@property (assign, nonatomic) int isSmoke;
@property (assign, nonatomic) int isPat;
@property (assign, nonatomic) int barrierFree;
@property (assign, nonatomic) int tv;
@property (assign, nonatomic) int bathroom;

-(id)initWithHouseID:(int)houseID shampoo:(int)shampoo aircondition:(int)aircondition kitchen:(int)kitchen wifi:(int)wifi hotwater:(int)hotwater parking:(int)parking elevator:(int)elevator isSmoke:(int)isSmoke isPat:(int)isPat barrierFree:(int)barrierFree tv:(int)tv bathroom:(int)bathroom;
+(id)FacilityWithHouseID:(int)houseID shampoo:(int)shampoo aircondition:(int)aircondition kitchen:(int)kitchen wifi:(int)wifi hotwater:(int)hotwater parking:(int)parking elevator:(int)elevator isSmoke:(int)isSmoke isPat:(int)isPat barrierFree:(int)barrierFree tv:(int)tv bathroom:(int)bathroom;

@end

