//
//  VOLHospitalityViewController.m
//  Volver
//
//  Created by administrator on 14-9-28.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLHospitalityViewController.h"

#import "common.h"
#import "REFrostedViewController.h"

#import "firstViewController.h"
#import "secondViewController.h"
#import "VOLTHirdViewController.h"
#import "fourthViewController.h"
#import "VOLFifthViewController.h"
#import "sixthViewController.h"
#import "VOLHospitalityCell.h"

@interface VOLHospitalityViewController (){
    NSDictionary *Hosdic ;
    NSArray *Hosarray ;
    NSArray *headtitle ;
}

@end

@implementation VOLHospitalityViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    //添加菜单按钮
    addLeftNavigationBarButton();
    
    self.title = @"好客之道" ;
    //打开plist文件（含有图片，标题信息）
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Hospitality" ofType:@"plist"];
    Hosdic = [NSDictionary dictionaryWithContentsOfFile:filePath];
    
    //获取hosdic所以的key
    Hosarray = [Hosdic allKeys];
    
    //设置tableview的背景图片
    UIImageView *img = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"hospitality-landing-bg.jpg"]];
    self.tableView.backgroundView = img ;
    
    //设置cell之间分割线的风格
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    //每个section的header视图的title数组
    headtitle = [[NSArray alloc]initWithObjects:@"沟通",@"为房客到来做准备",@"欢迎房客" ,nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark 显示菜单栏
-(void)showMenu:(id)sender{
    kActionOfMenuButton;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    
    //有几个key就有几个section
    return [Hosarray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    
    //通过key找到每个section里面包含的个数
    NSString *key = [Hosarray objectAtIndex:section];
    return [[Hosdic objectForKey:key]count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"HosCell";
    VOLHospitalityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if(cell == nil){
        cell = [[VOLHospitalityCell alloc]init];
    }
    //获得此section下的字典
    NSDictionary *hos = [Hosdic objectForKey:[Hosarray objectAtIndex:indexPath.section]];
    
    //获得字典下图片，标题的数组
    NSArray *im = [hos valueForKey:@"图片"];
    NSArray *til = [hos valueForKey:@"标题"];
    
    //从数组中获取本行的内容
    NSString *imagename = [im objectAtIndex:indexPath.row];
    NSString *title = [til objectAtIndex:indexPath.row];
    
    //填充自定义cell的内容
    cell.image.image= [UIImage imageNamed:imagename];
    cell.titleLabel.text = title ;
    cell.titleLabel.textColor = [UIColor whiteColor];
    
    //将cell背景颜色设置成透明
    cell.backgroundColor = [UIColor clearColor];
    
    //取消点击效果
    cell.selectionStyle = UITableViewCellSelectionStyleNone ;
    
    return cell;
    // Configure the cell...
    
   
}

//#pragma mark - 为每个section添加一个headerview
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    
//    UILabel *label = [[UILabel alloc]init];
//    label.frame = CGRectMake(0, 0, 120, 40);
//    label.backgroundColor = [[UIColor alloc]initWithWhite:0.9 alpha:0.7];
//    label.text = [headtitle objectAtIndex:section];
//    return label ;
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //根据indexpath获取secyion
    NSInteger section = indexPath.section ;
    
    if (section == 0) {
        switch (indexPath.row) {
            case 0:
            {
                
                firstViewController *first = [[firstViewController alloc]initWithNibName:@"firstViewController" bundle:nil];
                
                [self.navigationController pushViewController:first animated:YES];
                
            }
                break;
                
            case 1 :
            {
                secondViewController *second = [[secondViewController alloc]initWithNibName:@"secondViewController" bundle:nil];
                
                [self.navigationController pushViewController:second animated:YES];
            }
                break;
        }
    }
    else if(section == 1){
        switch (indexPath.row) {
            case 0:
            {
                VOLTHirdViewController *third = [[VOLTHirdViewController alloc]initWithNibName:@"VOLTHirdViewController" bundle:nil];
                
                [self.navigationController pushViewController:third animated:YES];        }
                
                break;
                
            case 1:
            {
                fourthViewController *four = [[fourthViewController alloc]initWithNibName:@"fourthViewController" bundle:nil];
                [self.navigationController pushViewController:four animated:YES];
            }
                
                break;
        }
        
    }
    else if(section == 2){
        switch (indexPath.row) {
            case 0:
            {
                VOLFifthViewController *fifth = [[VOLFifthViewController alloc]initWithNibName:@"VOLFifthViewController" bundle:nil];
                [self.navigationController pushViewController:fifth animated:YES];
            }
                
                break;
                
            case 1:
            {
                sixthViewController *six = [[sixthViewController alloc]initWithNibName:@"sixthViewController" bundle:nil];
                [self.navigationController pushViewController:six animated:YES];
            }
                
                break;
        }
        
    }
    
    
}

//返回每个cell的高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
