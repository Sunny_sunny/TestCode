//
//  FacilityIcon.h
//  ScrollViewTest
//
//  Created by administrator on 14-9-18.
//  Copyright (c) 2014年 bwj. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FacilityIcon : NSObject
@property (copy,nonatomic) NSString *imageName;
@property (copy,nonatomic) NSString *labelText;
-(id)initWithImageName:(NSString *)imageName labelText:(NSString *)labelText;
+(id)facilityIconWithImageName:(NSString *)imageName labelText:(NSString *)labelText;
@end
