//
//  VOLHireCell.h
//  Volver
//
//  Created by administrator on 14-10-18.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VOLHireCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *houseImageView;
@property (weak, nonatomic) IBOutlet UILabel *houseTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *guestNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *arriveDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *leaveDateLabel;

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;


@end
