//
//  User.h
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (assign, nonatomic) int userID;
@property (copy, nonatomic) NSString *userName;
@property (copy, nonatomic) NSString *userPhone;
@property (copy, nonatomic) NSString *userPassword;
@property (copy,nonatomic) NSString *userMail;
@property (copy,nonatomic) NSString *userSex;
@property (assign, nonatomic) double userBirth;
@property (copy,nonatomic) NSString *userHead;
@property (assign, nonatomic) double userRegisterTime;

-(id)initWithUserName:(NSString *)userName userPhone:(NSString *)userPhone userPassword:(NSString *)userPassword userRegister:(double)userRegisterTime;
+(id)userWithUserName:(NSString *)userName userPhone:(NSString *)userPhone userPassword:(NSString *)userPassword userRegister:(double)userRegisterTime;

@end
