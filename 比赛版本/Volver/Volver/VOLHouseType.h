//
//  VOLHouseType.h
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VOLHouseType : NSObject
@property (copy,nonatomic) NSString *iconImage;
@property (copy,nonatomic) NSString *type;
@property (copy,nonatomic) NSString *typeDesc;

-(id)initWithIconImage:(NSString *)iconImage type:(NSString *)type typeDesc:(NSString *)typeDesc;
+(id)houseTypeWithIconImage:(NSString *)iconImage type:(NSString *)type typeDesc:(NSString *)typeDesc;

@end
