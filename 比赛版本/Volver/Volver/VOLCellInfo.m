//
//  VOLCellInfo.m
//  Volver
//
//  Created by administrator on 14-9-17.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLCellInfo.h"

@implementation VOLCellInfo

-(id)initWithLeftText:(NSString *)leftText rightText:(NSString *)rightText{
    if (self = [super init]) {
        self.leftText = leftText;
        self.rightText = rightText;
    }
    return self;
}

+(id)cellInfoWithLeftText:(NSString *)leftText rightText:(NSString *)rightText{
    return [[self alloc] initWithLeftText:leftText rightText:rightText];
}

@end
