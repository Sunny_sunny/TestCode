//
//  VOLAddHouseImageViewController.h
//  CameraAndAlbumTest
//
//  Created by administrator on 14-9-26.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZYQAssetPickerController.h"

@interface VOLAddHouseImageViewController : UITableViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ZYQAssetPickerControllerDelegate>

@end
