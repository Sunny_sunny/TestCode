//
//  VOLTHirdViewController.h
//  Volver
//
//  Created by administrator on 14-9-25.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VOLTHirdViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@end
