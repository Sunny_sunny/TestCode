//
//  VOLHouseAllInfoViewController.h
//  Volver
//
//  Created by administrator on 14-9-16.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VOLHouseAllInfoViewController : UIViewController<UIScrollViewDelegate>
@property int sourceFrom;
@property int userID;
@property int houseID;

@end
