//
//  VOLChatUser.h
//  Volver
//
//  Created by administrator on 14-10-21.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMPPFramework.h"
@interface VOLChatUser : NSObject
@property (copy,nonatomic) NSString *name ;
@property (copy,nonatomic) NSString *pearence ;
@property int offlinemessage;
@property (copy,nonatomic) NSString *userid ;
@property (copy,nonatomic) NSString *headname;
@property (copy,nonatomic) NSString *volname ;
@end
