//
//  sixthViewController.m
//  haoke0925
//
//  Created by administrator on 14-9-25.
//  Copyright (c) 2014年 administrator. All rights reserved.
//

#import "sixthViewController.h"
#import "NSString+ext.h"
#import "common.h"
#define FONT_SIZE 15.0f
@interface sixthViewController ()
{
    NSMutableArray *array ;
    NSArray *imagearray ;
    UIFont *font ;
}
@end

@implementation sixthViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"支持";
    addBackButton();
    self.tableview.dataSource = self;
    self.tableview.delegate = self ;
    
    //添加内容数组
    array = [NSMutableArray array];
    [array addObject:@"尽量在房客入住当晚和房客确认所有事项，并在他们住宿期间随时保持联系。"];
    
    [array addObject:@"有时候，好房源也会出意外。请随时最好准备，并定期向他们通报补救工作的进展情况"];
    
    [array addObject:@"您可以为房源的失误之处做出弥补。无论是一张贴心的手写便条，还是帮助客户寻找新的住宿，都会向房客表达一种友好的心意。"];
    
    //创建图片数组
    imagearray = [[NSArray alloc]initWithObjects:@"HosApproachs.png",@"HosSolves.jpg" ,@"HosPerfects.png",nil];
    font = [UIFont fontWithName:@"Arial-BoldItalicMT" size:FONT_SIZE];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 返回
-(void) goBack:(id)sender{
    back;
}

#pragma mark - 返回行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [array count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //声明cell和label
    UITableViewCell *cell;
    UILabel *label ;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithFrame:CGRectZero];
    }
    
    //在cell里添加照片
    NSString *imagename = [imagearray objectAtIndex:indexPath.row];
    UIImage *im = [UIImage imageNamed:imagename];
    UIImageView *image = [[UIImageView alloc]initWithImage:im];
    image.frame= CGRectMake(10, 0, 300, 130);
    [[cell contentView ]addSubview:image];
    
    label = [[UILabel alloc] initWithFrame:CGRectZero];
    //当一行的最后一个单词如果写在那行中的话会超过参数size的width,令这个单词是写在下一行
    [label setLineBreakMode:NSLineBreakByWordWrapping];
    //不限制label行数
     label.numberOfLines = 0 ;
    //设置字体
    [label setFont:font];
    label.textColor = [UIColor darkGrayColor];
    //读取label的值
    NSString *text = [array objectAtIndex:[indexPath row]];
    //求出text的高度
    CGSize size = [text calculateSize:CGSizeMake(300, FLT_MAX) font:font];
    //设置label的frmae
    label.frame = CGRectMake(10, 155, size.width, size.height);
    [label setText:text];
    //添加label
    [[cell contentView] addSubview:label];
    //去掉点击效果
    cell.selectionStyle = UITableViewCellSelectionStyleNone ;
    
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *text = [array objectAtIndex:indexPath.row];
    CGSize size = [text calculateSize:CGSizeMake(300, FLT_MAX) font:font];
//    CGFloat height = [self boundingRectWithSize:CGSizeMake(320, 0) text:text].height;
    CGFloat height = size.height ;
    //返回值等于text的高度+图片的高度+图片与label中间的间隔
    return height  + 190 ;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tableview deselectRowAtIndexPath:indexPath animated:YES];
}

@end
