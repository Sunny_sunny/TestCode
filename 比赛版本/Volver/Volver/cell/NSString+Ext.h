//
//  NSString+Ext.h
//  TableviewAutoHeight
//
//  Created by Maggie on 14-9-24.
//  Copyright (c) 2014年 fmning. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Ext)
- (CGSize)calculateSize:(CGSize)size font:(UIFont *)font;
@end
