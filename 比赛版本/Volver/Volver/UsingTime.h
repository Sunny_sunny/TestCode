//
//  UsingTime.h
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UsingTime : NSObject

@property int usingTimeID;
@property int houseID;
@property int orderFormID;
@property double userArriveDate;
@property double userLeaveDate;

-(id)initWithusingtimeID:(int)usingtimeid andHouseId:(int)houseid andOrderFormId:(int) orderformid anduserarrivedate:(double)userarrivedate anduserleavedate:(double)userleavedate;
+(id)usingTimeWithusingtimeID:(int)usingtimeid andHouseId:(int)houseid andOrderFormId:(int) orderformid anduserarrivedate:(double)userarrivedate anduserleavedate:(double)userleavedate;
@end
