//
//  VOLHeadImageView.h
//  Volver
//
//  Created by admin on 14-10-8.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VOLHeadImageView : UIView<UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;

@end
