//
//  VOLChangePasswordViewController.h
//  Volver
//
//  Created by administrator on 14-11-1.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VOLChangePasswordViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *checkPwdField;
@property (weak, nonatomic) IBOutlet UIButton *affirmButton;


- (IBAction)changePassword:(id)sender;

@end
