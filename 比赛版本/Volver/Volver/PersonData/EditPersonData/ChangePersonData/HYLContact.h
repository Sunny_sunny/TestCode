//
//  HYLContact.h
//  Volver
//
//  Created by admin on 14-10-10.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HYLContact : NSObject

#pragma mark 姓
@property (nonatomic,copy) NSString *titleValue;
//#pragma mark 名
//@property (nonatomic,copy) NSString *lastName;
#pragma mark 手机号码
@property (nonatomic,copy) NSString *changeValue;

#pragma mark 带参数的构造函数
-(HYLContact *)initWithTitleValue:(NSString *)titleValue changeValue:(NSString *)changeValue;



#pragma mark 取得姓名
-(NSString *)getTitleValue;
-(NSString *)getChangeValue;

#pragma mark 带参数的静态对象初始化方法
+(HYLContact *)initWithTitleValue:(NSString *)titleValue changeValue:(NSString *)changeValue;

@end
