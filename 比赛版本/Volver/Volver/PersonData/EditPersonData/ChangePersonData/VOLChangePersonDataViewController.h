//
//  VOLChangePersonDataViewController.h
//  Volver
//
//  Created by admin on 14-10-8.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface VOLChangePersonDataViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,UIAlertViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,MBProgressHUDDelegate>
@property int sourceFrom;

@end
