//
//  HYLContactGroup.m
//  Volver
//
//  Created by admin on 14-10-10.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "HYLContactGroup.h"

@implementation HYLContactGroup

-(HYLContactGroup *)initWithName:(NSString *)name andDetail:(NSString *)detail andContacts:(NSMutableArray *)contacts{
    if (self=[super init]) {
        self.name=name;
        self.detail=detail;
        self.contacts=contacts;
    }
    return self;
}

+(HYLContactGroup *)initWithName:(NSString *)name andDetail:(NSString *)detail andContacts:(NSMutableArray *)contacts{
    HYLContactGroup *group1=[[HYLContactGroup alloc]initWithName:name andDetail:detail andContacts:contacts];
    return group1;
}

@end
