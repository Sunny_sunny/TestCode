//
//  HYLContact.m
//  Volver
//
//  Created by admin on 14-10-10.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "HYLContact.h"

@implementation HYLContact

-(HYLContact *)initWithTitleValue:(NSString *)titleValue changeValue:(NSString *)changeValue
{

    self = [super init];
    if (self) {
        self.titleValue = titleValue;
        self.changeValue = changeValue;
    }
    return self;
}

-(NSString *)getTitleValue{
    return [NSString stringWithFormat:@"%@",self.titleValue];
}

-(NSString *)getChangeValue{
    return [NSString stringWithFormat:@"%@",self.changeValue];
}

+(HYLContact *)initWithTitleValue:(NSString *)titleValue changeValue:(NSString *)changeValue
{

    HYLContact *contact = [[HYLContact alloc]initWithTitleValue:titleValue changeValue:changeValue];
    return contact;
}
@end
