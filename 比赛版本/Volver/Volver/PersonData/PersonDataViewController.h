//
//  PersonDataViewController.h
//  Volver
//
//  Created by admin on 14-10-8.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZYQAssetPickerController.h"

@interface PersonDataViewController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ZYQAssetPickerControllerDelegate>
@property int sourceFrom;
@property int userID;
@end
