//
//  Favorite.h
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Favorite : NSObject

@property (assign, nonatomic) int favoriteID;
@property int userID;
@property int houseID ;

-(id)initWithUserID:(int) userId andhouseID:(int)houseid;
+(id)FavoriteWithUserID:(int) userId andhouseID:(int)houseid;
@end
