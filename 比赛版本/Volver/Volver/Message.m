//
//  Message.m
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "Message.h"

@implementation Message
-(id)initWithsenderID:(int)senderid andReciverID:(int)reciverid andMessageContent:(NSString *)messagecontent andSendTime:(double)sendtime andisread:(int)isread{
    self = [super init];
    if (self) {
        self.senderID = senderid ;
        self.reciverID = reciverid ;
        self.messageContent = messagecontent ;
        self.sendTime = sendtime;
        self.isRead = isread ;
    }
    return self ;
}
+(id)messageWithsenderID:(int)senderid andReciverID:(int)reciverid andMessageContent:(NSString *)messagecontent andSendTime:(double)sendtime andisread:(int)isread{
    return [[self alloc]initWithsenderID:senderid andReciverID:reciverid andMessageContent:messagecontent andSendTime:sendtime andisread:isread];
}
@end
