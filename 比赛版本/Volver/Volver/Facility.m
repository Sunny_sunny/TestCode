//
//  Facility.m
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "Facility.h"

@implementation Facility

-(id)initWithHouseID:(int)houseID shampoo:(int)shampoo aircondition:(int)aircondition kitchen:(int)kitchen wifi:(int)wifi hotwater:(int)hotwater parking:(int)parking elevator:(int)elevator isSmoke:(int)isSmoke isPat:(int)isPat barrierFree:(int)barrierFree tv:(int)tv bathroom:(int)bathroom
{
    
    self = [super init];
    if (self) {
        self.houseID = houseID;
        self.shampoo = shampoo;
        self.aircondition = aircondition;
        self.kitchen = kitchen;
        self.wifi = wifi;
        self.hotwater = hotwater;
        self.parking = parking;
        self.elevator = elevator;
        self.isSmoke = isSmoke;
        self.isPat = isPat;
        self.barrierFree = barrierFree;
        self.tv = tv;
        self.bathroom = bathroom;
    }
    
    return self;
}

+(id)FacilityWithHouseID:(int)houseID shampoo:(int)shampoo aircondition:(int)aircondition kitchen:(int)kitchen wifi:(int)wifi hotwater:(int)hotwater parking:(int)parking elevator:(int)elevator isSmoke:(int)isSmoke isPat:(int)isPat barrierFree:(int)barrierFree tv:(int)tv bathroom:(int)bathroom
{
    
    return [[self alloc]initWithHouseID:houseID shampoo:shampoo aircondition:aircondition kitchen:kitchen wifi:wifi hotwater:hotwater parking:parking elevator:elevator isSmoke:isSmoke isPat:isPat barrierFree:barrierFree tv:tv bathroom:bathroom];
}

@end
