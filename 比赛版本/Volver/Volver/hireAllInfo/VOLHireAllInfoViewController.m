//
//  VOLHireAllInfoViewController.m
//  Volver
//
//  Created by administrator on 14-10-22.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLHireAllInfoViewController.h"

#import "common.h"
#import "HTTPDefine.h"

#import "ASIHTTPRequest.h"
#import "JSONKit.h"
#import "MBProgressHUD.h"

#import "VOLAppDelegate.h"
#import "VOLChatViewController.h"

@interface VOLHireAllInfoViewController ()
{
    VOLAppDelegate *del;
    JSONDecoder *jd;
    NSFileManager *manager;
    MBProgressHUD *HUD;
}
@end

@implementation VOLHireAllInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //初始化一些对象
    del = kGetDel;
    jd = [[JSONDecoder alloc] init];
    
    //标题
    self.title = @"订单详情";
    //添加返回按钮
    addBackButton();
    
    
    //设置label字体颜色
    self.arriveDateLabel.textColor = kOrangeColor;
    self.leaveDateLabel.textColor = kOrangeColor;
    
    //设置头像为圆形
    self.lodgerHeadImageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    self.lodgerHeadImageView.layer.masksToBounds = YES;
    self.lodgerHeadImageView.layer.cornerRadius = 50.0;
    self.lodgerHeadImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.lodgerHeadImageView.layer.borderWidth = 3.0f;
    self.lodgerHeadImageView.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.lodgerHeadImageView.layer.shouldRasterize = YES;
    self.lodgerHeadImageView.clipsToBounds = YES;
    
    //添加联系房客按钮
    UIButton *contactLodgerBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, kViewHeight - 44, kViewWidth, 44)];
    contactLodgerBtn.backgroundColor = [UIColor colorWithRed:1 green:90.0/255 blue:95.0/255 alpha:0.9];
    [contactLodgerBtn addTarget:self action:@selector(contactLodger:) forControlEvents:UIControlEventTouchUpInside];
    [contactLodgerBtn setTitle:@"联系房客" forState:UIControlStateNormal];
    [contactLodgerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [contactLodgerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [self.view addSubview:contactLodgerBtn];
    
    //加载数据
    [self loadHireAllInfo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 按钮点击事件
#pragma mark 返回事件
-(void)goBack:(id)sender{
    back;
}

#pragma mark 联系房客
-(void)contactLodger:(id)sender{
//    NSLog(@"联系房客操作");
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"电话",@"聊天", nil];
    [sheet showInView:self.view];
}

#pragma mark - 加载数据
#pragma mark 加载界面内容
-(void)loadHireAllInfo{
    NSString *urlstr = [NSString stringWithFormat:@"%@orderform/getHireAllInfo?orderFormID=%d",kHTTPHome,self.orderFormID];
    NSURL *url = [NSURL URLWithString:urlstr];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    request.delegate = self;
    [request startAsynchronous];
    
}

#pragma mark - 代理方法
#pragma mark - 异步请求代理
#pragma mark 请求成功
-(void)requestFinished:(ASIHTTPRequest *)request{
    //得到返回数据
    NSData *data = [request responseData];
    if (data == nil) {
        NSLog(@"loadHireAllInfo------data为空");
        return;
    }
    
    NSDictionary *dic = [jd objectWithData:data];
    int result = [[dic objectForKey:@"result"] intValue];
    if (result == 0) {
        NSLog(@"loadHireAllInfo------查询失败");
        return;
    }
    
    NSArray *array = [dic objectForKey:@"hireAllInfo"];
    NSDictionary *hireAllInfoDic = array[0];
    
    //更新界面内容
    self.lodgerNameLabel.text = [hireAllInfoDic objectForKey:@"user_name"];
    //判断性别是否为空
    NSString *sexStr = [hireAllInfoDic objectForKey:@"user_sex"];
    if ((NSNull *)sexStr == [NSNull null]) {
        self.lodgerSexLabel.text = @"暂无";
    }else{
        self.lodgerSexLabel.text = [hireAllInfoDic objectForKey:@"user_sex"];
    }
    
    
    self.lodgerPhoneLabel.text = [hireAllInfoDic objectForKey:@"user_phone"];
    self.lodgerNumLabel.text = [NSString stringWithFormat:@"%@位",[hireAllInfoDic objectForKey:@"person_num"]];
    float price = [[hireAllInfoDic objectForKey:@"totle_price"] doubleValue];
    self.totalPriceLabel.text = [NSString stringWithFormat:@"￥%.1f",price];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy年MM月dd日"];
    double arriveDate,leaveDate;
    arriveDate = [[hireAllInfoDic objectForKey:@"arrive_date"] doubleValue];
    leaveDate = [[hireAllInfoDic objectForKey:@"leave_date"] doubleValue];
    self.arriveDateLabel.text = [formatter stringFromDate:kTransformToDate(arriveDate)];
    self.leaveDateLabel.text = [formatter stringFromDate:kTransformToDate(leaveDate)];
    
    //加载房客头像
    NSString *headImageName = [hireAllInfoDic objectForKey:@"user_head"];
    int lodgerID = [[hireAllInfoDic objectForKey:@"user_id"] intValue];
    
    
    if ((NSNull *)headImageName != [NSNull null]) {
        //初始化manager
        manager = [NSFileManager defaultManager];
        //加载图片
        //先判断是否有这个路径存在，如果没有要先创建
        NSString *imageDocumentPath = [NSString stringWithFormat:@"%@%d/userHead/%d",kGetDocumentsPath,del.currentUser.userID,lodgerID];
        if (![manager fileExistsAtPath:imageDocumentPath]) {
            [manager createDirectoryAtPath:imageDocumentPath withIntermediateDirectories:YES attributes:nil error:nil];
        }
        //先从本地查找是否已有图片
        NSString *imagePath = [NSString stringWithFormat:@"%@%d/userHead/%d/%@",kGetDocumentsPath,del.currentUser.userID,lodgerID,headImageName];
        if ([manager fileExistsAtPath:imagePath]) {
            self.lodgerHeadImageView.image = [UIImage imageWithContentsOfFile:imagePath];
        }
        else{
            //若不存在，调用方法下载图片
            [del loadHeadImage:headImageName userID:lodgerID headImageView:self.lodgerHeadImageView imagePath:imagePath];
        }
    }
    else{
        self.lodgerHeadImageView.image = [UIImage imageNamed:@"defaultHead.png"];
    }
}


#pragma mark 请求失败
-(void)requestFailed:(ASIHTTPRequest *)request{
    NSLog(@"hire-----请求失败");
}

#pragma mark - ActionSheet代理
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        NSLog(@"打电话");
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",self.lodgerPhoneLabel.text]]];
    }
    else if (buttonIndex == 1){
        VOLChatViewController *ct = [[VOLChatViewController alloc]initWithNibName:@"VOLChatViewController" bundle:nil];
        ct.chatToName =[  NSString stringWithFormat:@"%@@aming.local",self.lodgerPhoneLabel.text] ;
        [self.navigationController pushViewController:ct animated:YES];
    }
}
@end









