//
//  VOLHireAllInfoViewController.h
//  Volver
//
//  Created by administrator on 14-10-22.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VOLHireCellModel.h"

@interface VOLHireAllInfoViewController : UIViewController<UIActionSheetDelegate>

@property int orderFormID;

@property (weak, nonatomic) IBOutlet UIImageView *lodgerHeadImageView;
@property (weak, nonatomic) IBOutlet UILabel *lodgerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lodgerSexLabel;
@property (weak, nonatomic) IBOutlet UILabel *lodgerPhoneLabel;

@property (weak, nonatomic) IBOutlet UILabel *lodgerNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *arriveDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *leaveDateLabel;




@end
