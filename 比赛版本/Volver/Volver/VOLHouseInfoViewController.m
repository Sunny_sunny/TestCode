//
//  VOLHouseInfoViewController.m
//  Volver
//
//  Created by administrator on 14-9-16.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLHouseInfoViewController.h"
#import "common.h"

#import "VOLAppDelegate.h"

@interface VOLHouseInfoViewController ()

@end

@implementation VOLHouseInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title = @"房间和床位";
    
    addBackButton();
    
    self.nextButton.backgroundColor = kThemeColor;
    
    //设置输入框的代理
    self.personNumberField.delegate = self;
    self.roomNumberField.delegate = self;
    self.bedNumberField.delegate = self;
    self.toiletField.delegate = self;
    
    self.personNumberField.textColor = kThemeColor;
    self.roomNumberField.textColor = kThemeColor;
    self.bedNumberField.textColor = kThemeColor;
    self.toiletField.textColor = kThemeColor;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark 返回按钮事件
-(void)goBack:(id)sender{
    back;
}

#pragma mark 跳转时记录参数
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    VOLAppDelegate *del = kGetDel;
    del.publishHouse.housePersons = [self.personNumberField.text intValue];
    del.publishHouse.houseBedroom = [self.roomNumberField.text intValue];
    del.publishHouse.houseBed = [self.bedNumberField.text intValue];
    del.publishHouse.houseToilet = [self.toiletField.text intValue];
}

#pragma mark TextField代理方法
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return NO;
}
#pragma mark 数字加1
- (IBAction)addNumber:(UIButton *)sender {
    int currentNumber;
    switch (sender.tag) {
        case 11:
            currentNumber = [self.personNumberField.text intValue];
            currentNumber++;
            self.personNumberField.text = [NSString stringWithFormat:@"%d",currentNumber];
            break;
        case 21:
            currentNumber = [self.roomNumberField.text intValue];
            currentNumber++;
            self.roomNumberField.text = [NSString stringWithFormat:@"%d",currentNumber];
            break;
        case 31:
            currentNumber = [self.bedNumberField.text intValue];
            currentNumber++;
            self.bedNumberField.text = [NSString stringWithFormat:@"%d",currentNumber];
            break;
        case 41:
            currentNumber = [self.toiletField.text intValue];
            currentNumber++;
            self.toiletField.text = [NSString stringWithFormat:@"%d",currentNumber];
            break;
            
    }
}
#pragma mark 数字减1
- (IBAction)subNumber:(UIButton *)sender {
    int currentNumber;
    switch (sender.tag) {
        case 10:
            currentNumber = [self.personNumberField.text intValue];
            if (currentNumber == 0) break;
            currentNumber--;
            self.personNumberField.text = [NSString stringWithFormat:@"%d",currentNumber];
            break;
        case 20:
            currentNumber = [self.roomNumberField.text intValue];
            if (currentNumber == 0) break;
            currentNumber--;
            self.roomNumberField.text = [NSString stringWithFormat:@"%d",currentNumber];
            break;
        case 30:
            currentNumber = [self.bedNumberField.text intValue];
            if (currentNumber == 0) break;
            currentNumber--;
            self.bedNumberField.text = [NSString stringWithFormat:@"%d",currentNumber];
            break;
        case 40:
            currentNumber = [self.toiletField.text intValue];
            if (currentNumber == 0) break;
            currentNumber--;
            self.toiletField.text = [NSString stringWithFormat:@"%d",currentNumber];
            break;
    }
}
@end






