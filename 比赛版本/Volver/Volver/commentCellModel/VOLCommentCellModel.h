//
//  VOLCommentCellModel.h
//  Volver
//
//  Created by administrator on 14-10-27.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VOLCommentCellModel : NSObject

@property int userID;
@property (copy,nonatomic) NSString *userHeadImageName;
@property (copy,nonatomic) NSString *userName;
@property (copy,nonatomic) NSString *content;
@property int score;
@property double commentTime;

@end
