//
//  HowUseViewController.m
//  Volver
//
//  Created by admin on 14-9-29.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "HowUseViewController.h"
#import "common.h"
#import "IntroControll.h"

@interface HowUseViewController ()

@end

@implementation HowUseViewController

- (id)init
{
    self = [super initWithNibName:nil bundle:nil];
//    self.wantsFullScreenLayout = YES;
    self.automaticallyAdjustsScrollViewInsets = YES;
    self.modalPresentationStyle = UIModalPresentationFullScreen;
    return self;
}

- (void) loadView {
    [super loadView];
    self.title = @"如何使用";
    addBackButton();
    IntroModel *model1 = [[IntroModel alloc] initWithTitle:@"快速租住全国最独特、迷人的房屋。" description:nil image:@"house_02_02.jpg"];
    IntroModel *model2 = [[IntroModel alloc] initWithTitle:@"找到最理想的住处" description:@"或随意浏览全国各地最新奇的特色住宿。" image:@"house_03_03.jpg"];
    IntroModel *model3 = [[IntroModel alloc] initWithTitle:@"轻松完成预定手续" description:@"房东接受您的预定后，您就可以准备启程了。" image:@"house_04_04.jpg"];
    IntroModel *model4 = [[IntroModel alloc] initWithTitle:@"即日启程" description:@"您可以在旅途中随时与房东进行联系，并查看您的行程单。" image:@"house_01_01.jpg"];
    self.view = [[IntroControll alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height + 64) pages:@[model1, model2, model3,model4]];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark 返回按钮事件
-(void)goBack:(id)sender{
    back;
}

@end
