//
//  fourthViewController.m
//  haoke0925
//
//  Created by administrator on 14-9-25.
//  Copyright (c) 2014年 administrator. All rights reserved.
//

#import "fourthViewController.h"
#import "NSString+ext.h"
#import "common.h"
#define FONT_SIZE 15.0f
@interface fourthViewController ()
{
    NSMutableArray *array ;
    NSArray *imagearray ;
    UIFont *font ;
}
@end

@implementation fourthViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"便利设施";
    addBackButton();
    self.tableview.dataSource = self;
    self.tableview.delegate = self ;
    
    //添加内容数组
    array = [NSMutableArray array];
    [array addObject:@"请准备好干净的毛巾，床单，洗手液，卫生纸，以及您房源描述中提到过的所有便利设施."];
    
    [array addObject:@"考虑提供洗发水，护发素，电吹风和熨斗。如果天色灰暗，在门边留一把雨伞。"];
    
    [array addObject:@"度身订造的欢迎礼物能为房客带来意外惊喜找出房客最喜爱的晨间饮品或睡前小酌的品种，以弄清您应当提供咖啡还是茶水，啤酒还是葡萄酒"];
    
    //创建图片数组
    imagearray = [[NSArray alloc]initWithObjects:@"HosCommoditys.png",@"HosExtras.png" ,@"HosLightspots.png",nil];
    font = [UIFont fontWithName:@"Arial-BoldItalicMT" size:FONT_SIZE];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 返回
-(void) goBack:(id)sender{
    back;
}

#pragma mark - 返回行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [array count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //声明cell和label
    UITableViewCell *cell;
    UILabel *label ;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithFrame:CGRectZero];
    }
    
    //在cell里添加照片
    NSString *imagename = [imagearray objectAtIndex:indexPath.row];
    UIImage *im = [UIImage imageNamed:imagename];
    UIImageView *image = [[UIImageView alloc]initWithImage:im];
    image.frame= CGRectMake(10, 0, 300, 130);
    [[cell contentView ]addSubview:image];
    
    label = [[UILabel alloc] initWithFrame:CGRectZero];
    //当一行的最后一个单词如果写在那行中的话会超过参数size的width,令这个单词是写在下一行
    [label setLineBreakMode:NSLineBreakByWordWrapping];
    //不限制label行数
    label.numberOfLines = 0 ;
    //设置字体
    [label setFont:font];
    label.textColor = [UIColor darkGrayColor];
    //读取label的值
    NSString *text = [array objectAtIndex:[indexPath row]];
    //求出text的高度
    CGSize size = [text calculateSize:CGSizeMake(300, FLT_MAX) font:font];
    //设置label的frmae
    label.frame = CGRectMake(10, 155, size.width, size.height);
    [label setText:text];
    //添加label
    [[cell contentView] addSubview:label];
    //去掉点击效果
    cell.selectionStyle = UITableViewCellSelectionStyleNone ;
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *text = [array objectAtIndex:indexPath.row];
    CGSize size = [text calculateSize:CGSizeMake(300, FLT_MAX) font:font];
    //    CGFloat height = [self boundingRectWithSize:CGSizeMake(320, 0) text:text].height;
    CGFloat height = size.height ;
    //返回值等于text的高度+图片的高度+图片与label中间的间隔
    return height  + 190 ;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tableview deselectRowAtIndexPath:indexPath animated:YES];
}
@end
