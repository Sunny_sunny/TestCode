//
//  Image.m
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "Image.h"

@implementation Image
-(id)initWithhouseID:(int)houseid andimageName:(NSString *)imagename{
    self = [super init];
    if (self) {
        self.houseID = houseid ;
        self.imageName = imagename ;
    }
    return self ;
}
+(id)imageWithhouseID:(int)houseid andimageName:(NSString *)imagename{
    return [[self alloc]initWithhouseID:houseid andimageName:imagename];
}
@end
