//
//  Comment.m
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "Comment.h"

@implementation Comment
-(id)initWithHouseID:(int)houseID andUseriD:(int)userid andContent:(NSString *)content andScore:(int)score andCommentTime:(double)commentTime{
    self = [super init];
    if (self) {
        
        self.houseID = houseID ;
        self.userID = userid ;
        self.content = content ;
        self.score = score ;
        self.commentTime = commentTime ;
    }
    return self ;
}
+(id)commentWithHouseID:(int)houseID andUseriD:(int)userid andContent:(NSString *)content andScore:(int)score andCommentTime:(double)commentTime{
    return [[self alloc]initWithHouseID:houseID andUseriD:userid andContent:content andScore:score andCommentTime:commentTime];
}

@end
