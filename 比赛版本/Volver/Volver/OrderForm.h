//
//  OrderForm.h
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderForm : NSObject

@property (assign, nonatomic) int orderFormID;
@property (assign, nonatomic) int userID;
@property (assign, nonatomic) int houseID;
@property (assign, nonatomic) double arriveDate;
@property (assign, nonatomic) double leaveDate;
@property (assign, nonatomic) int personNum;
@property (assign, nonatomic) float totlePrice;
@property (assign, nonatomic) int status;
@property (assign, nonatomic) int isComment;

-(id)initWithHouseID:(int)houseID arriveDate:(double)arriveDate leaveDate:(double)leaveDate status:(int)status isComment:(int)isComment;
+(id)orderWithHouseID:(int)houseID arriveDate:(double)arriveDate leaveDate:(double)leaveDate status:(int)status isComment:(int)isComment;

@end
