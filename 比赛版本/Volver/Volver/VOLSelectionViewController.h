//
//  VOLSelectionViewController.h
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"
#import <MapKit/MapKit.h>

@interface VOLSelectionViewController : UITableViewController<CLLocationManagerDelegate,MKMapViewDelegate>
@property int sourceFrom;
@property (copy,nonatomic) NSString *cityName;

@property (weak, nonatomic) IBOutlet UILabel *noticeLabel;


@end
