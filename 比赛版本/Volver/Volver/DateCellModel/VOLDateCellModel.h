//
//  VOLDateCellModel.h
//  Volver
//
//  Created by administrator on 14-10-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VOLDateCellModel : NSObject

@property double arriveDate;
@property double leaveDate;

-(id)initWithArrivtDate:(double)arriveDate leaveDate:(double)leaveDate;
+(id)dateCellModelWithArrivtDate:(double)arriveDate leaveDate:(double)leaveDate;

@end
