//
//  VOLDateCellModel.m
//  Volver
//
//  Created by administrator on 14-10-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLDateCellModel.h"

@implementation VOLDateCellModel

-(id)initWithArrivtDate:(double)arriveDate leaveDate:(double)leaveDate{
    if (self = [super init]) {
        self.arriveDate = arriveDate;
        self.leaveDate = leaveDate;
    }
    return self;
}

+(id)dateCellModelWithArrivtDate:(double)arriveDate leaveDate:(double)leaveDate{
    return [[self alloc] initWithArrivtDate:arriveDate leaveDate:leaveDate];
}
@end
