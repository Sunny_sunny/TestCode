//
//  BWJImageCell.h
//  CamaraTest
//
//  Created by administrator on 14-9-25.
//  Copyright (c) 2014年 bwj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BWJImageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *houseImageView;

@end
