//
//  OrderForm.m
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "OrderForm.h"

@implementation OrderForm

-(id)initWithHouseID:(int)houseID arriveDate:(double)arriveDate leaveDate:(double)leaveDate status:(int)status isComment:(int)isComment
{
    self = [super init];
    if (self) {
        self.houseID = houseID;
        self.arriveDate = arriveDate;
        self.leaveDate = leaveDate;
        self.status = status;
        self.isComment = isComment;
    }
    return self;
}

+(id)orderWithHouseID:(int)houseID arriveDate:(double)arriveDate leaveDate:(double)leaveDate status:(int)status isComment:(int)isComment
{
    
    return [[self alloc]initWithHouseID:houseID arriveDate:arriveDate leaveDate:leaveDate status:status isComment:isComment];
}

@end
