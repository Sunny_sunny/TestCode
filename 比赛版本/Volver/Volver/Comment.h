//
//  Comment.h
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Comment : NSObject
@property (assign, nonatomic) int commentID;
@property int houseID;
@property int userID;
@property (copy,nonatomic) NSString *content;
@property int score;
@property double commentTime;

-(id)initWithHouseID:(int)houseID andUseriD:(int)userid andContent:(NSString *)content andScore:(int)score andCommentTime:(double)commentTime;
+(id)commentWithHouseID:(int)houseID andUseriD:(int)userid andContent:(NSString *)content andScore:(int)score andCommentTime:(double)commentTime;
@end
