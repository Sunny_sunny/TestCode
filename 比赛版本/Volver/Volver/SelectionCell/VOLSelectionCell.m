//
//  VOLSelectionCell.m
//  Volver
//
//  Created by administrator on 14-10-11.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLSelectionCell.h"

@implementation VOLSelectionCell

NSMutableArray *imageViewArray;
UIImageView *imageView1;
UIImageView *imageView2;
UIImageView *imageView3;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark 获取scrollView上的三个ImageView
-(void)initImageView{
    NSArray *subviews = [self.imagesScrollView subviews];
    imageView1 = [subviews objectAtIndex:0];
    imageView2 = [subviews objectAtIndex:1];
    imageView3 = [subviews objectAtIndex:2];
}

-(void)removeAllImageView{
    NSArray *subViews = [self.imagesScrollView subviews];
    for (UIImageView *imageView in subViews) {
        [imageView removeFromSuperview];
    }
}

#pragma mark 初始化scorllView
-(void)setImageScrollVIew:(NSArray *)imageArray imageIndex:(int)imageIndex{
    
    CGFloat width = self.imagesScrollView.frame.size.width;
    CGFloat height = self.imagesScrollView.frame.size.height;
    int count = imageArray.count;
    
    //如果imageArray为空，直接返回
    if (count == 0) {
        return;
    }
 
    
    //如果scrollView上已经有了三个imageView，直接更改它们上面的图片，如果没有则创建
    if ([self.imagesScrollView subviews].count != 3) {
        //初始化scrollView的三个ImageView
        for (int i = 0; i < 3; i++) {
            UIImageView *imageView = [[UIImageView alloc] init];
            
            imageView.frame = CGRectMake(i * width, 0, width, height);
            imageView.contentMode = UIViewContentModeScaleToFill;
//            imageView.contentMode = UIViewContentModeScaleAspectFill;
            
            int imageNum = i - 1;
            if (imageNum < 0) {
                imageNum = imageArray.count - 1;
            }
            
            imageView.image = imageArray[imageNum % count];
            //将imageView加入到scrollView
            [self.imagesScrollView addSubview:imageView];
        }
        
        
        self.imagesScrollView.contentSize = CGSizeMake(3 * width, height);
        
        self.imagesScrollView.showsHorizontalScrollIndicator = NO;
        self.imagesScrollView.showsVerticalScrollIndicator = NO;
        
        self.imagesScrollView.pagingEnabled = YES;
        
        
    }else{
        [self initImageView];
        int imageNum = imageIndex - 1;
        if (imageNum < 0) {
            imageNum = imageArray.count - 1;
        }
        imageView1.image = imageArray[imageNum % count];
        imageView2.image = imageArray[imageIndex % count];
        imageView3.image = imageArray[(imageIndex + 1) % count];
    }
    
    self.imagesScrollView.contentOffset = CGPointMake(width, 0);
}


#pragma mark 滑动时，更新scrollView上的图片

-(void)updateImages:(NSArray *)imageArray index:(int)index direction:(int)direction{
    //获得scrollView上的三个imageView
    [self initImageView];
    
    int count = imageArray.count;
    
    if (direction == 1) {//向左滑动
//        NSLog(@"%@",imageArray[(index % count)]);
        imageView1.image = imageArray[(index % count)];
        imageView2.image = imageArray[((index + 1) % count)];
        imageView3.image = imageArray[((index + 2) % count)];
    }
    if (direction == 0) {//向右滑动
        imageView1.image = imageArray[(index - 2) % count];
        imageView2.image = imageArray[((index - 1) % count)];
        imageView3.image = imageArray[index % count];
        
    }
    self.imagesScrollView.contentOffset = CGPointMake(self.imagesScrollView.frame.size.width, 0);
    
}



@end
