//
//  VOLSelectionCell.h
//  Volver
//
//  Created by administrator on 14-10-11.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VOLSelectionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIScrollView *imagesScrollView;
@property (weak, nonatomic) IBOutlet UILabel *housePriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *houseTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *houseTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *houseCityLabel;
@property (weak, nonatomic) IBOutlet UILabel *houseScoreLabel;

//初始化scorllView
-(void)setImageScrollVIew:(NSArray *)imageArray imageIndex:(int)imageIndex;
//更新scrollView图片
-(void)updateImages:(NSArray *)imageArray index:(int)index direction:(int)direction;

@end
