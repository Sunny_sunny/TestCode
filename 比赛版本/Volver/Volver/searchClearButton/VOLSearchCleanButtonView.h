//
//  VOLSearchCleanButtonView.h
//  Volver
//
//  Created by administrator on 14-11-5.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VOLSearchCleanButtonView : UIView
@property (weak, nonatomic) IBOutlet UIButton *cleanButton;

@end
