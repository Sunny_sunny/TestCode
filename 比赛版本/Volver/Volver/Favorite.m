//
//  Favorite.m
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "Favorite.h"

@implementation Favorite
-(id)initWithUserID:(int)userId andhouseID:(int)houseid{
    self = [super init];
    if (self) {
        self.userID = userId ;
        self.houseID = houseid ;
    }
    return self ;
}
+(id)FavoriteWithUserID:(int)userId andhouseID:(int)houseid{
    return   [[self alloc]initWithUserID:userId andhouseID:houseid] ;
}

@end
