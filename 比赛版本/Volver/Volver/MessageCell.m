//
//  MessageCell.m
//  QQ聊天布局
//
//  Created by TianGe-ios on 14-8-19.
//  Copyright (c) 2014年 TianGe-ios. All rights reserved.
//

// 版权属于原作者
// http://code4app.com (cn) http://code4app.net (en)
// 发布代码于最专业的源码分享网站: Code4App.com

#import "MessageCell.h"
#import "CellFrameModel.h"
#import "MessageModel.h"
#import "UIImage+ResizeImage.h"
#import "VOLChatUser.h"
#import "common.h"
#import "HTTPDefine.h"
#import "VOLAppDelegate.h"

@interface MessageCell()
{
    UILabel *_timeLabel;
    UIImageView *_iconView;
    UIButton *_textView;
    VOLAppDelegate *del ;
    
}
@end

@implementation MessageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        _timeLabel.textColor = [UIColor grayColor];
        _timeLabel.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:_timeLabel];
        
        _iconView = [[UIImageView alloc] init];
        [self.contentView addSubview:_iconView];
        
        _textView = [UIButton buttonWithType:UIButtonTypeCustom];
        _textView.titleLabel.numberOfLines = 0;
        _textView.titleLabel.font = [UIFont systemFontOfSize:13];
        _textView.contentEdgeInsets = UIEdgeInsetsMake(textPadding, textPadding, textPadding, textPadding);
        [self.contentView addSubview:_textView];
    }
    return self;
}

- (void)setCellFrame:(CellFrameModel *)cellFrame
{
    
    del = kGetDel ;
    _cellFrame = cellFrame;
    MessageModel *message = cellFrame.message;
    
    _timeLabel.frame = cellFrame.timeFrame;
    _timeLabel.text = message.time;
    
    _iconView.frame = cellFrame.iconFrame;


    if (message.type ==1) {
        UIImage* headimage = [self loadheadimage:[self.user.userid intValue] andheadname:self.user.headname];
        _iconView.image = headimage ;
    }

    else{
     
        UIImage* headimages = [self loadheadimage:del.currentUser.userID andheadname:del.currentUser.userHead];
        _iconView.image = headimages;
    }
    
//    _iconView.image = headimage ;
    _textView.frame = cellFrame.textFrame;
    NSString *textBg = message.type ? @"chat_recive_nor" : @"chat_send_nor";
    UIColor *textColor = message.type ? [UIColor blackColor] : [UIColor whiteColor];
    [_textView setTitleColor:textColor forState:UIControlStateNormal];
    [_textView setBackgroundImage:[UIImage resizeImage:textBg] forState:UIControlStateNormal];
    [_textView setTitle:message.text forState:UIControlStateNormal];
}



//根据id 和头像名加载头像
-(UIImage *) loadheadimage:(int )userid andheadname:(NSString *)headname{
    
    
    UIImage *headimage = [[UIImage alloc]init];
    del = kGetDel ;
    if ((NSNull *)headname == [NSNull null]){

       headimage = [UIImage imageNamed:@"defaultHead.png"];

}
    else {
    
      
        //先判断是否有这个路径存在，如果没有要先创建
        NSFileManager *manager = [NSFileManager defaultManager];
        NSString *imageDocumentPath = [NSString stringWithFormat:@"%@%d/userHead/%d",kGetDocumentsPath,del.currentUser.userID,userid ];
        if (![manager fileExistsAtPath:imageDocumentPath]) {
            [manager createDirectoryAtPath:imageDocumentPath withIntermediateDirectories:YES attributes:nil error:nil];
        }
        
        
        //先从本地查找是否已有图片
        NSString *imagePath = [NSString stringWithFormat:@"%@%d/userHead/%d/%@",kGetDocumentsPath,del.currentUser.userID,userid ,headname];
        
        
        if ([manager fileExistsAtPath:imagePath]) {
//            landlordView.landlordHeadImageView.image = [UIImage imageWithContentsOfFile:imagePath];
            headimage = [UIImage imageWithContentsOfFile:imagePath] ;
        }
        else{
            //若不存在，调用方法下载图片
         headimage =   [self loadheadfff:userid andheadname:headname andpath:imagePath];
  
        }
}
    
    return headimage ;
    
    
}

#pragma mark 本地文件夹不存在该头像，加载头像图片

-(UIImage *) loadheadfff: (int )userid andheadname:(NSString *)headname andpath:(NSString *)imagepath{
    
 
  
    UIImage *image = [[UIImage alloc]init];
    
        //组装image的url
        NSString *urlstr = [NSString stringWithFormat:@"%@%d/%@",kUserHeadHome,userid,headname];
        NSURL *url = [NSURL URLWithString:urlstr];
        NSData *data = [NSData dataWithContentsOfURL:url];
        image = [UIImage imageWithData:data];
    
    
        if (image !=nil) {
            
            [data writeToFile:imagepath atomically:YES];
            
        }
    
        else{
 
            image = [UIImage imageNamed:@"defaultHead.png"];
            
        }
    
    return  image ;
    
    }

@end
