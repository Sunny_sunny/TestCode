//
//  VOLCommentViewController.h
//  Volver
//
//  Created by administrator on 14-10-20.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VOLCommentViewController : UIViewController<UITextViewDelegate>

@property int houseID;
@property int sourceFrom;

@property (weak, nonatomic) IBOutlet UITextView *contentTextView;

- (IBAction)gradeHouse:(UIButton *)sender;




@end
