//
//  VOLOrderFormInfoViewController.h
//  Volver
//
//  Created by administrator on 14-10-20.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VOLMyJourneyCellModel.h"
#import "VOLHistoryCellModel.h"

@interface VOLOrderFormInfoViewController : UIViewController<UIAlertViewDelegate,UIActionSheetDelegate>


@property int sourceFrom;
@property (retain, nonatomic) VOLMyJourneyCellModel* receiveModel;
@property (retain, nonatomic) VOLHistoryCellModel *historyModel;

@property (weak, nonatomic) IBOutlet UIImageView *houseImageView;
@property (weak, nonatomic) IBOutlet UILabel *houseTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *houseTypeAndPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *houseAddressLabel;

@property (weak, nonatomic) IBOutlet UILabel *landlordNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lodgerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *personNumLabel;

@property (weak, nonatomic) IBOutlet UILabel *arriveDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *leaveDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;

@property (weak, nonatomic) IBOutlet UILabel *ofStatusLabel;

@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet UILabel *isCommentLabel;

@property (weak, nonatomic) IBOutlet UILabel *noticeLabel;



@end
