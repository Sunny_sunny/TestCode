//
//  VOLHistoryOrderFormViewController.m
//  Volver
//
//  Created by administrator on 14-10-21.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLHistoryOrderFormViewController.h"

#import "common.h"
#import "HTTPDefine.h"

#import "REFrostedViewController.h"
#import "ASIHTTPRequest.h"
#import "JSONKit.h"
#import "MBProgressHUD.h"
#import "MJRefresh.h"

#import "VOLAppDelegate.h"
#import "VOLHistoryCell.h"
#import "VOLHistoryCellModel.h"
#import "VOLOrderFormInfoViewController.h"

@interface VOLHistoryOrderFormViewController ()
{
    VOLAppDelegate *del;
    JSONDecoder *jd;
    MBProgressHUD *HUD;
    NSFileManager *manager;
    
    //用来保存数据源的数组
    NSMutableArray *historyData;
}
@end

@implementation VOLHistoryOrderFormViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    //初始化一些对象
    del = kGetDel;
    jd = [[JSONDecoder alloc] init];
    manager = [NSFileManager defaultManager];
    //初始化房源数组
    historyData = [NSMutableArray array];
    
    
    //添加标题
    self.title = @"历史记录";
    
    //添加返回按钮
    addBackButton();
    
    //添加编辑按钮
    UIBarButtonItem *editBtn = [[UIBarButtonItem alloc] initWithTitle:@"编辑" style:UIBarButtonItemStylePlain target:self action:@selector(edit:)];
    [editBtn setTintColor:kThemeColor];
    self.navigationItem.rightBarButtonItem = editBtn;
    //默认失效
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    //将noticeLabel隐藏，当需要显示时再显示出来
    self.noticeLabel.hidden = YES;
    
    
    //添加上拉下拉；
    [self setupRefresh];
    
    //开启多行编辑功能
    self.tableView.allowsMultipleSelectionDuringEditing = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 进入界面时刷新
-(void)viewWillAppear:(BOOL)animated{
    if (del.isRefresh == 0) {//当不是从菜单栏或者发布成功时跳转过来的不用刷新
        return;
    }
    
    if (del.isRefresh == 20) {//从“我的旅程”跳转过来时
        //将是否刷新再置为0
        del.isRefresh = 0;
        
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:HUD];
        [HUD showAnimated:YES whileExecutingBlock:^{
            //将是否刷新再置为0
            del.isRefresh = 0;
            [self loadHistoryOrderForm];
        } completionBlock:^{
            [HUD removeFromSuperview];
            HUD = nil;
        }];
    }
    else{//
        //将是否刷新再置为0
        del.isRefresh = 0;
        
        //开始刷新
        [self.tableView headerBeginRefreshing];
    }
    
}

#pragma mark - 其它方法
#pragma mark - 设置上拉、下拉
- (void)setupRefresh
{
    // 1.下拉刷新(进入刷新状态就会调用self的headerRereshing)
    [self.tableView addHeaderWithTarget:self action:@selector(hisHeaderRereshing)];
    
    
    // 2.上拉加载更多(进入刷新状态就会调用self的footerRereshing)
    //    [self.tableView addFooterWithTarget:self action:@selector(footerRereshing)];
    
    // 设置文字(也可以不设置,默认的文字在MJRefreshConst中修改)
    self.tableView.headerPullToRefreshText = @"下拉刷新";
    self.tableView.headerReleaseToRefreshText = @"松开马上刷新了";
    self.tableView.headerRefreshingText = @"拼命加载中...";
    
}
#pragma mark - 下拉刷新时调用的方法
-(void)hisHeaderRereshing{
    //加载数据，用异步方式
    [self loadHistoryOrderForm];
}


#pragma mark - 按钮点击事件
#pragma mark 返回
-(void)goBack:(id)sender{
    back;
}

#pragma mark 编辑动作
-(void)edit:(id)sender{
    BOOL isEditing = !self.tableView.editing;
    if (isEditing) {
        [self.tableView setEditing:isEditing animated:YES];
        self.navigationItem.rightBarButtonItem.title = @"删除";
    }else{
        
//        NSLog(@"执行删除");
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:HUD];
        [HUD showAnimated:YES whileExecutingBlock:^{
            [self deleteOrderForm];
        } completionBlock:^{
            [HUD removeFromSuperview];
            HUD = nil;
        }];
        
        
        self.navigationItem.rightBarButtonItem.title = @"编辑";
        [self.tableView setEditing:isEditing animated:YES];
    }
}

#pragma mark 删除订单操作
-(void)deleteOrderForm{
    //获取被选中的cell的indexPath，
    NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
    if (selectedRows.count == 0) {
        return;
    }
    
    //创建数组，将删除成功的indexPath加入其中，删除cell时用到
    NSMutableArray *deletedArray = [NSMutableArray array];
    
    //创建一个堆，将删除成功的indexPath.row加入其中，删除historyData中的数据时用到
    NSMutableIndexSet *deletedSet = [NSMutableIndexSet new];
    
    //遍历数组删除数据
    for (NSIndexPath *indexPath in selectedRows) {
        VOLHistoryCellModel *model = historyData[indexPath.row];
        
        //连接网络删除订单表中的内容
        int isDelete = [self deleteFromOrderFormTable:model.orderFormID];
        
        if (isDelete) {//删除成功
            //将indexPath加入到deletedArray中
            [deletedArray addObject:indexPath];
            //将indexPath.row加入到deletedSet中
            [deletedSet addIndex:indexPath.row];
        }
        
    }
    
    if (deletedArray.count == 0) {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //遍历结束时删除对应的数据和cell
        //先删除数据
        [historyData removeObjectsAtIndexes:deletedSet];
        //再删除cell
        [self.tableView deleteRowsAtIndexPaths:deletedArray withRowAnimation:UITableViewRowAnimationAutomatic];
        
        
        //删除完之后如果historyData为空，要将编辑按钮失效
        if (historyData.count == 0) {
            self.navigationItem.rightBarButtonItem.enabled = NO;
        }
    });
    

}

#pragma mark 连网删除订单表中的内容
-(int)deleteFromOrderFormTable:(int)orderFormID{
    NSString *urlstr = [NSString stringWithFormat:@"%@orderform/deleteOrderForm?orderFormID=%d",kHTTPHome,orderFormID];
    NSURL *url = [NSURL URLWithString:urlstr];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request startSynchronous];
    
    NSData *data = [request responseData];
    if (data) {
        NSDictionary *dic = [jd objectWithData:data];
        int result = [[dic objectForKey:@"result"] intValue];
        return result;
    }else{
        NSLog(@"deleteFromOrderFormTable------data为空");
        return 0;
    }
}


#pragma mark - 加载数据
#pragma mark 异步请求加载历史记录数据
-(void)loadHistoryOrderForm{
    //使用异步请求加载
    
    //发送异步请求
    
    NSString *urlstr = [NSString stringWithFormat:@"%@orderform/searchAllFinishdOrderFormsByUserID?userID=%d",kHTTPHome,del.currentUser.userID];
    NSURL *url = [NSURL URLWithString:urlstr];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    //设置代理
    request.delegate = self;
    [request startAsynchronous];
}

#pragma mark 加载图片
-(void)loadImage:(NSString *)imageName houseID:(int)houseID indexPath:(NSIndexPath *)indexPath imagePath:(NSString *)imagePath{
    
    //开启新线程加载图片
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //组装image的url
        NSString *urlstr = [NSString stringWithFormat:@"%@%d/%@",kHouseImageHome,houseID,imageName];
        NSURL *url = [NSURL URLWithString:urlstr];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *image = [UIImage imageWithData:data];
        if (image !=nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                VOLHistoryCell *cell = (VOLHistoryCell *)[self.tableView cellForRowAtIndexPath:indexPath];
                cell.houseImageView.image = image;
                //本地保存图片
                [data writeToFile:imagePath atomically:YES];
            });
        }
        else{
            NSLog(@"没有图片");
        }
    });
    
}

#pragma mark - 代理方法

#pragma mark - 异步请求代理
#pragma mark 请求成功
-(void)requestFinished:(ASIHTTPRequest *)request{
    //得到返回数据
    NSData *data = [request responseData];
    if (data == nil) {
        NSLog(@"historyOF------data为空");
        return;
    }
    
    NSDictionary *dic = [jd objectWithData:data];
    int result = [[dic objectForKey:@"result"] intValue];
    if (result == 0) {
        NSLog(@"historyOF-----查询失败");
    }
    else{
        //首先判断historyData是否为空，如果不是，先清空
        if (historyData.count != 0) {
            [historyData removeAllObjects];
        }
        
        //得到订单数组
        NSArray *array = [dic objectForKey:@"historyOrderForms"];
        
        //遍历这个数组
        for (NSDictionary *obj in array) {
            VOLHistoryCellModel *model = [[VOLHistoryCellModel alloc] init];
            model.orderFormID = [[obj objectForKey:@"of_id"] intValue];
            model.houseID = [[obj objectForKey:@"house_id"] intValue];
            model.isComment = [[obj objectForKey:@"is_comment"] intValue];
            //房屋标题和价格
            model.houseTitle = [obj objectForKey:@"house_title"];
            model.housePrice = [[obj objectForKey:@"house_price"] floatValue];
            
            kGetHouseType(model.houseType, [[obj objectForKey:@"house_type"] intValue]);
            model.houseImageName = [obj objectForKey:@"image_name"];
            
            //将model加入到myJourneyData
            [historyData addObject:model];
        }
    }
    
    
    
    //数组遍历完毕，更新主界面
    //判断是否有收藏的房屋
    if (historyData.count == 0) {
        //显示提示Label
        self.noticeLabel.text = @"您还没有住房记录哦~~~";
        self.noticeLabel.textColor = kThemeColor;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.noticeLabel.hidden = NO;
    }else{
        //编辑按钮有效
        self.navigationItem.rightBarButtonItem.enabled = YES;
        self.noticeLabel.hidden = YES;
    }
    
    //刷新tableView
    [self.tableView reloadData];
    
    //停止刷新动作
    [self.tableView headerEndRefreshing];
}


#pragma mark 请求失败
-(void)requestFailed:(ASIHTTPRequest *)request{
    //停止刷新动作
    [self.tableView headerEndRefreshing];
    NSLog(@"historyOF-----请求失败");
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return historyData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"historyCell";
    VOLHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    cell.houseTypeAndPriceLabel.textColor = kPriceLabelColor;
    cell.isCommentLabel.textColor = kOrangeColor;
    
    VOLHistoryCellModel *model = historyData[indexPath.row];
    cell.houseTitleLabel.text = model.houseTitle;
    cell.houseTypeAndPriceLabel.text = [NSString stringWithFormat:@"%@ · ￥%.1f/晚",model.houseType,model.housePrice];
    
    //判断是否已评价
    if (model.isComment) {
        cell.isCommentLabel.textColor = kPriceLabelColor;
        cell.isCommentLabel.text = @"已评价";
    }else{
        cell.isCommentLabel.textColor = kOrangeColor;
        cell.isCommentLabel.text = @"未评价";
    }
    
    //加载图片
    //先判断是否有这个路径存在，如果没有要先创建
    NSString *imageDocumentPath = [NSString stringWithFormat:@"%@%d/houseImages/%d",kGetDocumentsPath,del.currentUser.userID,model.houseID];
    if (![manager fileExistsAtPath:imageDocumentPath]) {
        [manager createDirectoryAtPath:imageDocumentPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    //先从本地查找是否已有图片
    NSString *imagePath = [NSString stringWithFormat:@"%@%d/houseImages/%d/%@",kGetDocumentsPath,del.currentUser.userID,model.houseID,model.houseImageName];
    if ([manager fileExistsAtPath:imagePath]) {
        cell.houseImageView.image = [UIImage imageWithContentsOfFile:imagePath];
    }
    else{
        //若不存在，调用方法下载图片
        [self loadImage:model.houseImageName houseID:model.houseID indexPath:indexPath imagePath:imagePath];
    }
    return cell;
}

#pragma mark 行高
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 105;
}

#pragma mark 点击cell时
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.tableView.editing) {
        return;
    }
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    //跳转
    VOLOrderFormInfoViewController *orderFormInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"orderFormInfoController"];
    orderFormInfo.historyModel = historyData[indexPath.row];
    orderFormInfo.sourceFrom = 26;
    [self.navigationController pushViewController:orderFormInfo animated:YES];
}
/*
#pragma mark 滑动删除
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle != UITableViewCellEditingStyleDelete) {
        return;
    }
}
*/
@end






