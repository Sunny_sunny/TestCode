//
//  VOLHouseType.m
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLHouseType.h"

@implementation VOLHouseType

-(id)initWithIconImage:(NSString *)iconImage type:(NSString *)type typeDesc:(NSString *)typeDesc{
    if (self = [super init]) {
        self.iconImage = iconImage;
        self.type = type;
        self.typeDesc = typeDesc;
    }
    return self;
}

+(id)houseTypeWithIconImage:(NSString *)iconImage type:(NSString *)type typeDesc:(NSString *)typeDesc{
    return [[self alloc] initWithIconImage:iconImage type:type typeDesc:typeDesc];
}

@end
