//
//  VOLHospitalityCell.h
//  Volver
//
//  Created by administrator on 14-9-28.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VOLHospitalityCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;


@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
