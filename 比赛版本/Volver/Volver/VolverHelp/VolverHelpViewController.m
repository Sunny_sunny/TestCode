//
//  VolverHelpViewController.m
//  Volver
//
//  Created by admin on 14-9-26.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VolverHelpViewController.h"
#import "common.h"
#import "Cell1.h"
#import "NSString+Ext.h"

@interface VolverHelpViewController ()
{
    NSMutableArray *stringArray;
}

@end

@implementation VolverHelpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad]; 
    self.title = @"Volver帮助中心";
	addBackButton();
    //获取一个单元格对象
    stringArray = [NSMutableArray arrayWithObjects:@"Volver客户端可提供线上下单、取消预定或编辑您的个人资料，帮助文章描述了Volver移动网站以及应用的相关信息，“好客之道”提供给您在发布房间时的“小贴士”。\n\nVolver尚处在1.0版本，很多细节我们或许做的不够完美、极致，如您在使用过程中遇到“闪退”或者您认为需要我们改进的地方，请您联系我们。\n\n我们的联系方式：\n邮箱:huangyulei2013@126.com\n", nil];
    self.helpTableView.dataSource = self;
    self.helpTableView.delegate = self;
    self.helpTableView.separatorStyle = UITableViewCellSeparatorStyleNone;

}

#pragma mark 返回按钮事件
-(void)goBack:(id)sender{
    back;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark  section数量
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

#pragma mark section的行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [stringArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableViewCell *cell = [[UITableViewCell alloc]init];
    cell.textLabel.text = [stringArray objectAtIndex:[indexPath row]];
    cell.textLabel.numberOfLines = 0;
    return cell;
}
#pragma mark cellForRow
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    UIFont *font = [UIFont fontWithName:@"Arial-BoldItalicMT" size:20];
   return  [[stringArray objectAtIndex:indexPath.row]  calculateSize:CGSizeMake(300, FLT_MAX) font:font  ].height +40 ;
    

}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return 30;
}

@end
