//
//  HTTPDefine.h
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#ifndef Volver_HTTPDefine_h
#define Volver_HTTPDefine_h
#define kHTTPRoot @"http://10.110.3.85:8888/volverAPP/"
#define kHTTPHome @"http://10.110.3.85:8888/volverAPP/index.php/home/"
#define kHost @"http://localhost:8888/volverAPP/"

#define kHouseImageHome @"http://10.110.3.85:8888/volverAPP/Uploads/houseImages/"
#define kUserHeadHome @"http://10.110.3.85:8888/volverAPP/Uploads/userHead/"

#pragma mark - XMPP服务器地址
#define kXMPPID @"10.110.3.85"
#define kXMPPLocal @"@baiwenji.local"

#endif
