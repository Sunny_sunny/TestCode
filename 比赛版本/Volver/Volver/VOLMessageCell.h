//
//  VOLMessageCell.h
//  Volver
//
//  Created by administrator on 14-10-28.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VOLMessageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headimage;
@property (weak, nonatomic) IBOutlet UILabel *xmppNamelabel;
@property (weak, nonatomic) IBOutlet UILabel *xmpppearencelabel;
@property (weak, nonatomic) IBOutlet UILabel *offlinemsglabel;
@property (weak, nonatomic) IBOutlet UILabel *messagelabel;

@end
