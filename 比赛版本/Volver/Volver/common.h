//
//  common.h
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#ifndef Volver_common_h
#define Volver_common_h
#import "MD5.h"

#pragma mark 获取AppDelegate对象
#define kGetDel [[UIApplication sharedApplication] delegate]


#pragma mark 获取当前view的宽和高
#define kViewWidth [self.view bounds].size.width
#define kViewHeight [self.view bounds].size.height

#pragma mark 头像圆弧半径
#define kCompressionRatio 0.5

#pragma mark 一天的秒数
#define kOneDaySeconds 86400

#pragma mark 添加手势的方法
#define kAddPanGestureRecognizer [self.view addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognized:)]]

#pragma mark 手势处理方法
#define kHandlePanGesture [self.frostedViewController panGestureRecognized:sender]

#pragma mark 菜单按钮点击事件处理方法
#define kActionOfMenuButton [self.frostedViewController presentMenuViewController]

#pragma mark 主题色
#define kThemeColor [UIColor colorWithRed:1 green:90/255.0 blue:95/255.0 alpha:1]

#pragma mark 黑色
#define kBlackColor [UIColor blackColor]

#pragma mark 菜单栏字体默认颜色
#define kMenuTextDefaultColor [UIColor colorWithRed:62/255.0f green:68/255.0f blue:75/255.0f alpha:1.0f]

#pragma mark 价格Label的背景色
#define kPriceLabelColor [UIColor colorWithRed:65.0/255 green:65.0/255 blue:65.0/255 alpha:0.8]

#pragma mark 入住时间Label字体颜色
#define kOrangeColor [UIColor colorWithRed:246.0/255 green:129.0/255 blue:0 alpha:1]


#pragma mark 给导航栏添加菜单按钮
#define addLeftNavigationBarButton()\
UIButton *menuBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 18, 16)];\
[menuBtn addTarget:self action:@selector(showMenu:) forControlEvents:UIControlEventTouchUpInside];\
[menuBtn.imageView setContentMode:UIViewContentModeScaleAspectFill];\
[menuBtn setBackgroundImage:[UIImage imageNamed:@"menuIcon.png"] forState:UIControlStateNormal];\
self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuBtn];\


#pragma mark 导航栏添加返回按钮
#define addBackButton()\
UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 12)];\
[backBtn addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];\
[backBtn.imageView setContentMode:UIViewContentModeScaleAspectFill];\
[backBtn setBackgroundImage:[UIImage imageNamed:@"backIcon.png"] forState:UIControlStateNormal];\
self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];\




#pragma mark 返回按钮的事件
#define back [self.navigationController popViewControllerAnimated:YES]


#pragma mark 获取当前时间，并转化为秒数
#define kGetCurrentTime [[NSDate date] timeIntervalSince1970]

#pragma mark 将一个秒数转化成一个NSDate对象
#define kTransformToDate(time) [NSDate dateWithTimeIntervalSince1970:time]


#pragma mark 获取房屋类型
#define kGetHouseType(string,type)\
switch(type){\
case 0:string = @"整套房子";break;\
case 1:string = @"独立房间";break;\
case 2:string = @"合租房间";break;\
}

#pragma mark 获取沙盒路径
#define kGetDocumentsPath [NSString stringWithFormat:@"%@/Documents/",NSHomeDirectory()]

#endif
