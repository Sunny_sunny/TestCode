//
//  User.m
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "User.h"

@implementation User

-(id)initWithUserName:(NSString *)userName userPhone:(NSString *)userPhone userPassword:(NSString *)userPassword userRegister:(double)userRegisterTime
{
    
    self = [super init];
    if (self) {
        self.userName = userName;
        self.userPhone = userPhone;
        self.userPassword = userPassword;
        self.userRegisterTime = userRegisterTime;
    }
    
    return self;
}

+(id)userWithUserName:(NSString *)userName userPhone:(NSString *)userPhone userPassword:(NSString *)userPassword userRegister:(double)userRegisterTime
{
    
    return [[self alloc]initWithUserName:userName userPhone:userPhone userPassword:userPassword userRegister:userRegisterTime];
}

@end
