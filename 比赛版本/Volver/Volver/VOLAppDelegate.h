//
//  VOLAppDelegate.h
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "House.h"
#import "Facility.h"
#import "User.h"
#import "VOLChatUser.h"

#import "XMPPFramework.h"
#import "HTTPDefine.h"
#import "Comment.h"
#import "FMDatabase.h"
#import "Reachability.h"

@interface VOLAppDelegate : UIResponder <UIApplicationDelegate>
{
    Reachability *reach;
}
@property (retain,nonatomic) XMPPStream *xmppsteam ; //xmpp流
@property (retain,nonatomic) NSMutableArray *rosters ; //我的好友数组（聊过天的）
@property (retain ,nonatomic) XMPPRosterCoreDataStorage *xmpprosterdatastorge;
@property (retain,nonatomic)XMPPRoster *xmpproster ; //花名册
@property (retain,nonatomic) NSMutableDictionary *offlinemessagedic ;//存储各个好友的离线消息数量的字典

//是否登录
@property BOOL isLogin;
//是否自动登录
@property int isAutologin;
//进入界面是否刷新
@property int isRefresh;
//菜单栏当前被选中的cell的indexPath
@property (retain, nonatomic) NSIndexPath *lastIndex;

@property (retain, nonatomic) House *publishHouse;
@property (retain, nonatomic) Facility *publishFacility;
//计数被选中的便利设施的个数
@property int facilityCount;
@property (retain, nonatomic) NSMutableArray *selectedHouseImages;

@property (retain, nonatomic) User *currentUser;

@property (strong, nonatomic) UIWindow *window;

@property (copy,nonatomic) NSString *pwd ; //登陆的用户名
@property (copy,nonatomic) NSString *username;  //登陆的用户密码

-(int)userLoginUserPhone:(NSString *)userPhone userPassword:(NSString *)userPassword;

#pragma mark 加载头像图片
-(void)loadHeadImage:(NSString *)imageName userID:(int)userID headImageView:(UIImageView *)headImageView imagePath:(NSString *)imagePath;

-(NSString *)getDBPath;//加载保存聊天记录数据库的路径

-(void) updatepassword:(NSString *)name :(NSString *)pwd ;//修改用户的密码


-(void)getUseHead:(VOLChatUser *)user ;

@end
