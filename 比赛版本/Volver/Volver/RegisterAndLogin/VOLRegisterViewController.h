//
//  VOLRegisterViewController.h
//  Volver
//
//  Created by administrator on 14-9-19.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMPPFramework.h"

@interface VOLRegisterViewController : UIViewController<UITextFieldDelegate,XMPPStreamDelegate,XMPPRosterDelegate>
@property (weak, nonatomic) IBOutlet UITextField *phoneField;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *verifyPasswdField;


- (IBAction)closeView:(id)sender;

- (IBAction)userRegister:(id)sender;


@end
