//
//  VOLLoginViewController.h
//  Volver
//
//  Created by administrator on 14-9-20.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VOLLoginViewController : UIViewController<UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet UITextField *phoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *passwordLabel;


- (IBAction)goToRegister:(id)sender;
- (IBAction)userLogin:(id)sender;
- (IBAction)closeView:(id)sender;



@end
