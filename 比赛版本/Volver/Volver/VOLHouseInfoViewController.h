//
//  VOLHouseInfoViewController.h
//  Volver
//
//  Created by administrator on 14-9-16.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VOLHouseInfoViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *personNumberField;
@property (weak, nonatomic) IBOutlet UITextField *roomNumberField;
@property (weak, nonatomic) IBOutlet UITextField *bedNumberField;
@property (weak, nonatomic) IBOutlet UITextField *toiletField;

@property (weak, nonatomic) IBOutlet UIButton *nextButton;

- (IBAction)addNumber:(UIButton *)sender;

- (IBAction)subNumber:(UIButton *)sender;


@end
