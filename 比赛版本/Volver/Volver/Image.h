//
//  Image.h
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Image : NSObject

@property (assign, nonatomic) int imageID;
@property int houseID ;
@property (copy,nonatomic) NSString *imageName;

-(id)initWithhouseID:(int) houseid andimageName:(NSString *)imagename;
+(id)imageWithhouseID:(int) houseid andimageName:(NSString *)imagename;
@end
