//
//  VOLHistoryCell.h
//  Volver
//
//  Created by administrator on 14-10-21.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VOLHistoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *houseImageView;
@property (weak, nonatomic) IBOutlet UILabel *houseTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *houseTypeAndPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *isCommentLabel;



@end
