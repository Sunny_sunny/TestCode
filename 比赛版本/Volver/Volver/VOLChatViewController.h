//
//  VOLChatViewController.h
//  Volver
//
//  Created by administrator on 14-10-21.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMPPFramework.h"
@interface VOLChatViewController : UIViewController<XMPPStreamDelegate,XMPPRosterDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UITextField *chatfield;
@property (copy ,nonatomic) NSString *chatToName;
@property (copy,nonatomic) NSString *userid ;
- (IBAction)sendmessage:(id)sender;

@end
