//
//  Message.h
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject

@property (assign, nonatomic) int messageID;
@property int senderID;
@property int reciverID;
@property (copy,nonatomic)NSString *messageContent ;
@property double sendTime;
@property int isRead ;

-(id)initWithsenderID:(int)senderid andReciverID:(int) reciverid andMessageContent:(NSString *)messagecontent andSendTime:(double) sendtime andisread:(int) isread;
+(id)messageWithsenderID:(int)senderid andReciverID:(int) reciverid andMessageContent:(NSString *)messagecontent andSendTime:(double) sendtime andisread:(int) isread;
@end
