//
//  VOLCellInfo.h
//  Volver
//
//  Created by administrator on 14-9-17.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VOLCellInfo : NSObject

@property (copy,nonatomic) NSString *leftText;
@property (copy,nonatomic) NSString *rightText;
@property int mark;

-(id)initWithLeftText:(NSString *)leftText rightText:(NSString *)rightText;
+(id)cellInfoWithLeftText:(NSString *)leftText rightText:(NSString *)rightText;

@end
