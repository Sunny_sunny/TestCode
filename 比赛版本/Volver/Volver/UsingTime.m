//
//  UsingTime.m
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "UsingTime.h"

@implementation UsingTime
-(id)initWithusingtimeID:(int)usingtimeid andHouseId:(int)houseid andOrderFormId:(int)orderformid anduserarrivedate:(double)userarrivedate anduserleavedate:(double)userleavedate {
    self = [super init];
    if (self) {
        self.usingTimeID = usingtimeid;
        self.houseID = houseid ;
        self.orderFormID = orderformid ;
        self.userArriveDate = userarrivedate;
        self.userLeaveDate = userleavedate ;
    }
    return self  ;
}
+(id)usingTimeWithusingtimeID:(int)usingtimeid andHouseId:(int)houseid andOrderFormId:(int)orderformid anduserarrivedate:(double)userarrivedate anduserleavedate:(double)userleavedate{
    
    return [[self alloc]initWithusingtimeID:usingtimeid andHouseId:houseid andOrderFormId:orderformid anduserarrivedate:userarrivedate anduserleavedate:userleavedate];
}
@end
