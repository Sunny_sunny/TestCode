//
//  VOLHireCellModel.h
//  Volver
//
//  Created by administrator on 14-10-18.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VOLHireCellModel : NSObject

@property int orderFormID;
@property int houseID;
@property (copy,nonatomic) NSString *houseTitle;
@property int userID;
@property (copy,nonatomic) NSString *userName;
@property int status;
@property double arriveDate;
@property double leaveDate;
@property (copy,nonatomic) NSString *houseImageName;

@end
