//
//  VOLDescCell.h
//  Volver
//
//  Created by administrator on 14-10-28.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VOLDescCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end
