//
//  MD5.m
//  EncryptTest
//
//  Created by administrator on 14-11-4.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "MD5.h"
#import "CommonCrypto/CommonDigest.h"

@implementation MD5

+ (NSString *)md5HexDigest:(NSString *)password
{
    const char *original_str = [password UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(original_str, strlen(original_str), result);
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < 16; i++)
        
    {
        [hash appendFormat:@"%X", result[i]];
    }
    NSString *mdfiveString = [hash lowercaseString];
    return mdfiveString;
}


@end
