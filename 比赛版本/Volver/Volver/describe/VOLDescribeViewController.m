//
//  VOLDescribeViewController.m
//  Volver
//
//  Created by administrator on 14-10-28.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLDescribeViewController.h"

#import "common.h"
#import "NSString+ext.h"

#import "VOLDescCell.h"

@interface VOLDescribeViewController ()
{
    NSMutableArray *contentData;
    VOLDescCell *prototyCell;
}
@end

@implementation VOLDescribeViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //设置标题
    self.title = @"房屋描述";
    //添加返回按钮
    addBackButton();

    prototyCell = [self.tableView dequeueReusableCellWithIdentifier:@"descCell"];
    self.tableView.bounces = NO;
    //SectionHeader的高度
    self.tableView.sectionHeaderHeight = 34;
    
    //初始化字典并加入到数组中
    NSDictionary *descDic = @{
                              @"title":@"描述",
                              @"content":self.houseDesc
                              };
    NSDictionary *nearDic = @{
                              @"title":@"附近",
                              @"content":self.houseNear
                              };
    NSDictionary *trafficDic = @{
                                 @"title":@"交通",
                                 @"content":self.houseTraffic
                                 };
    contentData = [NSMutableArray arrayWithObjects:descDic,nearDic,trafficDic, nil];
    
    //添加tablefooter
    self.tableView.tableFooterView = [[UILabel alloc] init];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 按钮点击事件
#pragma mark 返回
-(void)goBack:(id)sender{
    back;
}


#pragma mark - 代理方法
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return contentData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"descCell";
    VOLDescCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *dic = contentData[indexPath.section];
    NSString *content = dic[@"content"];
    //判断content是否为空
    if ([content isEqualToString:@""]) {
        content = @"房东很懒，暂时还没有";
    }
    cell.contentLabel.text = content;
    
    //动态调整label高度
    CGSize newSize = [content calculateSize:CGSizeMake(cell.contentLabel.frame.size.width, FLT_MAX) font:cell.contentLabel.font];
    CGRect rect = cell.contentLabel.frame;
    rect.size = newSize;
    cell.contentLabel.frame = rect;
    
    return cell;
}


#pragma mark cell预设高度
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}
#pragma mark cell真实高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    VOLDescCell *cell = (VOLDescCell *)prototyCell;
    
    //当前字符串
    NSString *content = [contentData[indexPath.section] objectForKey:@"content"];
    
    //获取label动态高度
    CGSize newSize = [content calculateSize:CGSizeMake(cell.contentLabel.frame.size.width, FLT_MAX) font:cell.contentLabel.font];
    
    CGFloat defaultHeight = cell.contentView.frame.size.height;
    
    CGFloat height = newSize.height > defaultHeight ? newSize.height : defaultHeight;
    
    //返回cell高度
    return height + cell.contentLabel.frame.origin.y + 5;
}

#pragma mark 自定义Section的Header
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)sectionIndex
{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 34)];
    view.backgroundColor = [UIColor colorWithRed:239/255.0f green:239/255.0f blue:244/255.0f alpha:0.6f];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 8, 0, 0)];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = [contentData[sectionIndex] objectForKey:@"title"];
    label.font = [UIFont systemFontOfSize:15];
    label.textColor = kThemeColor;
    label.backgroundColor = [UIColor clearColor];
    [label sizeToFit];
    [view addSubview:label];
    
    return view;
}

@end
