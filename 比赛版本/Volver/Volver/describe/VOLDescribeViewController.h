//
//  VOLDescribeViewController.h
//  Volver
//
//  Created by administrator on 14-10-28.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VOLDescribeViewController : UITableViewController

@property (copy,nonatomic) NSString *houseDesc;
@property (copy,nonatomic) NSString *houseNear;
@property (copy,nonatomic) NSString *houseTraffic;


@end
