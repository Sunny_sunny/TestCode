//
//  BWJHouseOwnerView.h
//  ScrollViewTest
//
//  Created by administrator on 14-9-18.
//  Copyright (c) 2014年 bwj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BWJHouseOwnerView : UIView

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *registerTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *publishTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *stopTimeLabel;




@property (weak, nonatomic) IBOutlet UIButton *moreButton;


@end
