//
//  BWJHouseImageShowVIew.h
//  ScrollViewTest
//
//  Created by administrator on 14-9-18.
//  Copyright (c) 2014年 bwj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BWJHouseImageShowVIew : UIView

@property (weak, nonatomic) IBOutlet UIScrollView *imageScrollView;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;


@end
