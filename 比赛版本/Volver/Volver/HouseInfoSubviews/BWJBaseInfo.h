//
//  BWJBaseInfo.h
//  ScrollViewTest
//
//  Created by administrator on 14-9-18.
//  Copyright (c) 2014年 bwj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BWJBaseInfo : UIView

@property (weak, nonatomic) IBOutlet UILabel *personNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *roomNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *bedNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *toiletNumLabel;

@end
