//
//  VOLGoThereViewController.h
//  Volver
//
//  Created by admin on 14-10-23.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
//#import <AMapSearchKit/AMapSearchAPI.h>
#import "VOLHouseAllInfoViewController.h"

@interface VOLGoThereViewController : UIViewController<CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *mapWebVIew;
@property int houseID ;


@end
