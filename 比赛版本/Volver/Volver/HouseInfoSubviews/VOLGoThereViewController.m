//
//  VOLGoThereViewController.m
//  Volver
//
//  Created by admin on 14-10-23.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLGoThereViewController.h"
#import "VOLAppDelegate.h"
#import "common.h"
#import "HTTPDefine.h"
#import <CoreLocation/CoreLocation.h>

#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "JSONKit.h"


#define key @"f139551450d4970194492a925c6b420c"
#define NavigationHTTP @"http://mo.amap.com/navi/?"         //导航请求
#define CodeHTTP @"http://restapi.amap.com/v3/geocode/geo?"     //地理编码

@interface VOLGoThereViewController ()
{

    VOLAppDelegate *del;
    float destLatitude;
    float destLongitude;
    float currentLongitude;
    float currentLagitude;
    NSString *destName;
    CLLocationManager *locationManager;
    NSString *addressString;
    JSONDecoder *json;
}

@end

@implementation VOLGoThereViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"路径规划";
    addBackButton();
    //定位,创建位置管理器
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLHeadingFilterNone;
    //开始定位
    [locationManager startUpdatingLocation];
    //初始化json
    json = [[JSONDecoder alloc] init];
    //查询houseID的信息
    [self getHouseInfo:self.houseID];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark 返回按钮事件
-(void)goBack:(id)sender{
    back;
}

#pragma mark 根据传过来的houseID查询房屋信息
-(void)getHouseInfo:(int)houseID
{
    NSString *urlString = [NSString stringWithFormat:@"%@House/information",kHTTPHome];
    NSURL *url = [NSURL URLWithString:urlString];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setPostValue:[NSString stringWithFormat:@"%d",self.houseID] forKey:@"houseID"];
    [request setRequestMethod:@"POST"];
    [request startSynchronous];
    NSData *data = [request responseData];
    if (data) {
        NSDictionary *dic = [json objectWithData:data];
        NSArray *array = [dic objectForKey:@"house"];
        NSDictionary *houseDic = [array objectAtIndex:0];
        int status = [[dic objectForKey:@"status"] intValue];
        if (status == 1) {
            NSString *stringPro = [houseDic objectForKey:@"house_province"];
            NSString *stringCity = [houseDic objectForKey:@"house_city"];
            NSString *stringStreet = [houseDic objectForKey:@"house_street"];
            destName = [NSString stringWithFormat:@"%@%@",stringCity,stringStreet];
            destLatitude = [[houseDic objectForKey:@"house_latitude"] floatValue];
            destLongitude = [[houseDic objectForKey:@"house_longitude"] floatValue];
            addressString = [NSString stringWithFormat:@"%@%@%@",stringPro,stringCity,stringStreet];
        }
    }
}



#pragma mark 将定位到的经纬度分别赋给currentLongitude、currentLagitude
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    
    CLLocation *location = [locations firstObject];
    double longitude = location.coordinate.longitude;
    double latitude = location.coordinate.latitude;
    [locationManager stopUpdatingLocation];
    //分别将纬度经度赋给currentLongitude、currentLagitude
    currentLongitude = longitude;
    currentLagitude = latitude;
    [self loadMap];
    
    
}

-(void)loadMap
{
    NSString *stringURL = [NSString stringWithFormat:@"%@start=%f,%f&dest=%f,%f&destName=%@&key=%@",NavigationHTTP,currentLongitude,currentLagitude,destLongitude,destLatitude,destName,key];
    NSString *string = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *URL = [NSURL URLWithString:string];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    [self.mapWebVIew loadRequest:request];
    NSLog(@"destLatitude:%f,destLongitude:%f",destLatitude,destLongitude);
}





@end
