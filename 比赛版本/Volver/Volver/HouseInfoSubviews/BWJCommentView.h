//
//  BWJCommentView.h
//  ScrollViewTest
//
//  Created by administrator on 14-9-18.
//  Copyright (c) 2014年 bwj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BWJCommentView : UIView

@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;


@end
