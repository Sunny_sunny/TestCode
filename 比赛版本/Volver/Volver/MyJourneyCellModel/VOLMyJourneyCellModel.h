//
//  VOLMyJourneyCellModel.h
//  Volver
//
//  Created by administrator on 14-10-17.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VOLMyJourneyCellModel : NSObject

@property int orderFormID;
@property int houseID;
@property double arriveDate;
@property (copy,nonatomic) NSString *houseTitle;
@property (copy,nonatomic) NSString *houseAddress;
@property (copy,nonatomic) NSString *houseImageName;

@end
