//
//  VOLContentViewController.m
//  Volver
//
//  Created by administrator on 14-9-15.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLContentViewController.h"
#import "common.h"
#import "REFrostedViewController.h"

@interface VOLContentViewController ()

@end

@implementation VOLContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    //给内容控制器添加侧滑手势
    kAddPanGestureRecognizer;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark 手势识别
-(void)panGestureRecognized:(UIPanGestureRecognizer *)sender{
    CGPoint point = [sender translationInView:self.view];
    if (point.x > 0 && self.viewControllers.count == 1) {
        kHandlePanGesture;
        return;
    }
    if (point.x > 50 && sender.state == UIGestureRecognizerStateEnded) {
        [self popViewControllerAnimated:YES];
        
    }

}


@end
