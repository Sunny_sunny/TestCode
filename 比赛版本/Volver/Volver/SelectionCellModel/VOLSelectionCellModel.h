//
//  VOLSelectionCellModel.h
//  Volver
//
//  Created by administrator on 14-10-11.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VOLSelectionCellModel : NSObject

@property int imageIndex;
@property (retain, nonatomic) NSMutableArray *imageNameArray;
@property (retain, nonatomic) NSMutableArray *imageArray;

@property int houseID;
@property float housePrice;
@property (copy,nonatomic) NSString *houseTitle;
@property (copy,nonatomic) NSString *houseType;
@property (copy,nonatomic) NSString *houseCity;
@property float houseScore;
@property int houseFacility;
@property int houseOwner;

@end
