//
//  VOLTHirdViewController.m
//  Volver
//
//  Created by administrator on 14-9-25.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLTHirdViewController.h"
#import "NSString+ext.h"
#import "common.h"
#define FONT_SIZE 15.0f
@interface VOLTHirdViewController ()
{
    NSMutableArray *array ;
    NSArray *imagearray ;
    UIFont *font ;
}
@end

@implementation VOLTHirdViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"清洁";
    addBackButton();
    self.tableview.dataSource = self;
    self.tableview.delegate = self ;
    
    //添加内容数组
    array = [NSMutableArray array];
    [array addObject:@"井然有序的空间看起来会更清爽，更能吸引房客入住，舒适的空间是由许多微小的细节组成的。"];
    
    [array addObject:@"打扫您的房子，让它干净得足以让您的母亲引以为豪，为房客提供刚洗干净，没有污渍的床上用品和毛巾。"];
    
    [array addObject:@"请在下一位房客抵达之前，抽出足够的时间确保房源处于最佳状态。"];
    
    //创建图片数组
    imagearray = [[NSArray alloc]initWithObjects:@"HosfirstImpressions.png",@"HosZones.png" ,@"HosServices.png",nil];
    font = [UIFont fontWithName:@"Arial-BoldItalicMT" size:FONT_SIZE];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - 返回
-(void) goBack:(id)sender{
    back;
}
#pragma mark - 返回行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [array count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //声明cell和label
    UITableViewCell *cell;
    UILabel *label ;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithFrame:CGRectZero];
    }
    
    //在cell里添加照片
    NSString *imagename = [imagearray objectAtIndex:indexPath.row];
    UIImage *im = [UIImage imageNamed:imagename];
    UIImageView *image = [[UIImageView alloc]initWithImage:im];
    image.frame= CGRectMake(10, 0, 300, 130);
    [[cell contentView ]addSubview:image];
    
    label = [[UILabel alloc] initWithFrame:CGRectZero];
    //当一行的最后一个单词如果写在那行中的话会超过参数size的width,令这个单词是写在下一行
    [label setLineBreakMode:NSLineBreakByWordWrapping];
    //不限制label行数
    label.numberOfLines = 0 ;
    //设置字体
    [label setFont:font];
    label.textColor = [UIColor darkGrayColor];
    //读取label的值
    NSString *text = [array objectAtIndex:[indexPath row]];
    //求出text的高度
    CGSize size = [text calculateSize:CGSizeMake(300, FLT_MAX) font:font];
    //设置label的frmae
    label.frame = CGRectMake(10, 155, size.width, size.height);
    [label setText:text];
    //添加label
    [[cell contentView] addSubview:label];
    //去掉点击效果
    cell.selectionStyle = UITableViewCellSelectionStyleNone ;
    
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //运用拓展类里面的方法来计算text的高度
    NSString *text = [array objectAtIndex:indexPath.row];
    CGSize size = [text calculateSize:CGSizeMake(300, FLT_MAX) font:font];
    CGFloat height = size.height ;
    
    //返回值等于text的高度+图片的高度+图片与label中间的间隔+label与底部之间的间隔
    return height  + 190 ;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tableview deselectRowAtIndexPath:indexPath animated:YES];
}
@end
