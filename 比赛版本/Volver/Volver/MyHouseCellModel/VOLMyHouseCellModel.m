//
//  VOLMyHouseCellModel.m
//  Volver
//
//  Created by administrator on 14-9-29.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import "VOLMyHouseCellModel.h"

@implementation VOLMyHouseCellModel

-(id)initWithHouseImageName:(NSString *)houseImageName houseTitle:(NSString *)houseTitle houseType:(NSString *)houseType houseAddress:(NSString *)houseAddress{
    if (self = [super init]) {
        self.houseImageName = houseImageName;
        self.houseTitle = houseTitle;
        self.houseType = houseType;
        self.houseAddress = houseAddress;
    }
    return self;
}

+(id)myHouseCellModelWithHouseImageName:(NSString *)houseImageName houseTitle:(NSString *)houseTitle houseType:(NSString *)houseType houseAddress:(NSString *)houseAddress{
    return [[self alloc] initWithHouseImageName:houseImageName houseTitle:houseTitle houseType:houseType houseAddress:houseAddress];
}


@end
