//
//  VOLMyHouseCellModel.h
//  Volver
//
//  Created by administrator on 14-9-29.
//  Copyright (c) 2014年 vol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VOLMyHouseCellModel : NSObject

@property int houseID;
@property (copy,nonatomic) NSString *houseImageName;
@property (copy,nonatomic) NSString *houseTitle;
@property (copy,nonatomic) NSString *houseType;
@property (copy,nonatomic) NSString *houseAddress;
@property (assign, nonatomic) double houseExpirationdate;

-(id)initWithHouseImageName:(NSString *)houseImageName houseTitle:(NSString *)houseTitle houseType:(NSString *)houseType houseAddress:(NSString *)houseAddress;
+(id)myHouseCellModelWithHouseImageName:(NSString *)houseImageName houseTitle:(NSString *)houseTitle houseType:(NSString *)houseType houseAddress:(NSString *)houseAddress;

@end
